FROM openjdk:8-jdk-alpine
COPY target/shopping-list.jar app.jar
ENTRYPOINT ["java", "-jar","/app.jar"]