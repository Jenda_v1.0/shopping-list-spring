import React, {Component} from 'react';
import './App.css';
import List from './todo_list/components/List';

/**
 * "Hlavní" komponenta.
 */

export default class App extends Component {
    render() {
        return (
            <div className="App">
                <h1>todos</h1>
                <List/>
            </div>
        );
    }
}