// Značí typ položky, hodnoty (db_value) stejné jako v cz.uhk.fim.shoppinglist.entity.KindOfShoppingItem

// Výchozí je první hodnota

// (Pouze pro snazší testování:)
export const TASK_DB_VALUE = "TASK";

export const KINDS_OF_SHOPPING_ITEM = [
    {
        value: "Task",
        label: "Task",
        db_value: TASK_DB_VALUE
    },
    {
        value: "Description",
        label: "Description",
        db_value: "DESCRIPTION"
    }
];