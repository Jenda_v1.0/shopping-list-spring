// Priorita položky (do určité míry lze brát jako kategorie):

// ("none" je výchozí hodnota bez priority, která značí, že to vůbec nespěchá ("je to jedno"), v případě změny změnit i testování v ExpansionPanelItem.)

// (První hodnota je brána jako výchozí priorita - none bez uvedení priority)

export const PRIORITIES = [
    {
        // Prázdné hodnoty, když nebude priorita vůbec zadaná:
        value: "",
        label: "",
        db_value: null
    },
    {
        value: "none",
        label: "none",
        db_value: "NONE"
    },
    {
        value: "must be fulfilled",
        label: "must be fulfilled",
        db_value: "MUST_BE_FULFILLED"
    },
    {
        value: "necessarily",
        label: "necessarily",
        db_value: "NECESSARILY"
    },
    {
        value: "good to meet",
        label: "good to meet",
        db_value: "GOOD_TO_MEET"
    },
    {
        value: "hurries",
        label: "hurries",
        db_value: "HURRIES"
    },
    {
        value: "does not hurry",
        label: "does not hurry",
        db_value: "DOES_NOT_HURRY"
    },
    {
        value: "when it is fulfilled it will be fulfilled",
        label: "when it is fulfilled it will be fulfilled",
        db_value: "WHEN_IT_IS_FULFILLED_IT_WILL_BE_FULFILLED"
    }
];