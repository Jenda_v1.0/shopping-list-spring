import {UNITS} from "./Units";
import {PRIORITIES} from "./Priorities";

const unitDefault = UNITS[0].value;
const priorityDefault = PRIORITIES[0].db_value;

// Nalezení jednotky (konstanty) dle její databázové podoby (unit = db_value) a získaní její textové podoby pro zobrazení uživateli v rozevírací nabídce (value):
export const getTextValueByKindOfUnitInDbValue = (unit, value) => {
    // Pokud nebyly zadány jednotky, vrátí se výchozí:
    if (unit === undefined || unit === null) {
        return unitDefault;
    }

    // Nalezení názvu jednotky dle jejího typu (resp. vrátí se unit.value místo unit.db_value):
    const temp = UNITS.find(element => {
        return element.db_value === unit;
    });
    // return temp.value;
    return temp[value];
};

// Nalezení jednotky (konstanty) dle její databázové podoby (priority = db_value) a získání její textové podoby pro zobrazení uživateli v rozevírací nabídce (value):
export const getTextValueByPriorityInDbValue = priority => {
    // Pokud nebyla zadána priorita, vrátí se výchozí:
    if (priority === undefined || priority === null) {
        return priorityDefault;
    }

    // Nalezení názvu jednotky dle jejího typu (resp. vrátí se unit.value místo unit.db_value):
    const temp = PRIORITIES.find(element => {
        return element.db_value === priority;
    });
    return temp.value;
};

// Získání databázové hodnoty pro prioritu priority, což bude "value" hodnoty příslušné priority:
// (priority bude PRIORITIES.value, a má se vrátit PRIORITIES.db_value)
// (šlo by i , že by se do funkce getTextValueByPriorityInDbValue předávaly parametry odkud se mají brát hodnoty z PRIORITIES a jaká hodnota se z toho má vracet)
export const getDbPriorityValueByPriorityValue = priority => {
    // Pokud nebyla zadána priorita, vrátí se výchozí:
    if (priority === undefined || priority === null) {
        return priorityDefault;
    }

    // Nalezení názvu priority dle jejího typu (resp. vrátí se unit.db_value místo unit.value):
    const temp = PRIORITIES.find(element => {
        return element.value === priority;
    });
    return temp.db_value;
};

// Získání databázové hodonty pro množství:
// - (pokud bude množství prázdný text, vrátí se null hodnota, která se vloží do DB, aby se vědělo, že uživatel nezadal množství)
// - (pokud bude množství konkrétní číslo, vrátí se to číslo)
export const getDbValueForAmount = amount => {
    if (amount === "") {
        return null;
    }
    return amount;
};