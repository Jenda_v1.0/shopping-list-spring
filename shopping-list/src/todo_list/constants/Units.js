// Konstanty značí typy jednotek / množství. Například 5 kusů jako 5 rohlíků apod.

// (První hodnota je brána jako výchozí typ hodnoty, dle které se budou filtrovat položky:

export const UNITS = [
    {
        // Prázdná hodnota, když nebude jednotka vůbec zadaná:
        value: "",
        label: "",
        db_value: null
    },
    {
        value: "piece",
        label: "ks",
        db_value: "PIECE"
    },
    {
        value: "kilogram",
        label: "kg",
        db_value: "KILOGRAM"
    },
    {
        value: "decagram",
        label: "dkg",
        db_value: "DECAGRAM"
    },
    {
        value: "gramme",
        label: "g",
        db_value: "GRAMME"
    },
    {
        value: "pound",
        label: "lb",
        db_value: "POUND"
    }
];