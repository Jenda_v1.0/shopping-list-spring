import React from 'react';
import TextField from '@material-ui/core/TextField';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ImageButton from './ImageButton';
import AddItemIcon from '../images/addItem.png';
import EditIcon from '../images/editItem.png';
import CancelEditingIcon from '../images/cancelEditItem.png';
import DeleteAllItemsIcon from '../images/deleteAllItemsIcon.png';
import BackspaceIcon from '../images/backspace.png';
import {PRIORITIES} from '../constants/Priorities';
import {UNITS} from '../constants/Units';
import SelectTextField from "./SelectTextField";
import './styles/FormItem.css';
import {KINDS_OF_SHOPPING_ITEM} from '../constants/KindOfShoppingItem';
import {
    getDbValueForAmount,
    getTextValueByKindOfUnitInDbValue,
    getTextValueByPriorityInDbValue
} from "../constants/Helper";

// Výchozí hodnoty (pouze ty, které je třeba nastavit například při přidání položky apod. - ty které se němění):
const valueInTextInputItemNameDefault = "";
const itemTextInputHelperTextDefault = "Item name";
const multilineNoteDefault = "";
const amountDefault = "";
const unitDefault = UNITS[0].value;// Souvisí s: kindOfUnitDefault
const kindOfUnitDefault = UNITS[0].db_value;// Souvisí s: unitDefault
const itemAmountErrorDefault = false;
const itemAmountErrorHelperTextDefault = "Number of items";
const deadLineErrorDefault = false;
const deadlineHelperTextDefault = "Deadline to complete item";
const priorityTextValueDefault = PRIORITIES[0].value;// Souvisí s: priorityDefault
const priorityDefault = PRIORITIES[0].db_value;// Souvisí s: priorityTextValueDefault
const kindOfShoppingItemDefault = KINDS_OF_SHOPPING_ITEM[0].db_value;// Souvisí s kindOfShoppingItemDefaultTextValue
const kindOfShoppingItemDefaultTextValue = KINDS_OF_SHOPPING_ITEM[0].value;// Souvisí s kindOfShoppingItemDefault

/**
 * Formulář pro zadání hodnot / vlastností nové položky.
 *
 * Je povinné zadat název položky. Zbylé hodnoty v rozevíracím menu jsou volitelné.
 *
 * Info pro práci s datem a časem:
 * https://stackoverflow.com/questions/49277112/react-js-how-to-set-a-default-value-for-input-date-type
 */

export default class FormItem extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // Hodnota v textovém poli pro zadání názvu item:
            valueInTextInputItemName: valueInTextInputItemNameDefault,
            // True v případě, že pole obsahuje chybnou hodnotu, nebo žádnou apod.:
            // (false, je že neobsahuje chybu, a nebude obarveno pole, ale mělo by být true, proto je kontrola vaidní hodnoty přd přidáním položky)
            errorTextInputItemName: false,
            // Text zobrazen pod textovým polem, bude zobrazovat info o nevalidní hodnotě nebo, že se má zadat název Item:
            itemTextInputHelperText: itemTextInputHelperTextDefault,


            // Následující již nejsou hodnoty týkající se textového pole pro název položky:

            // Hodnota v textovém poli pro poznámku k item:
            multilineNote: multilineNoteDefault,
            // Bude zabalené rozevírací menu s podrobnostmi / další informace o item:
            expanded: undefined,
            // Množství položek, například 5 rohlíků apod.:
            amount: amountDefault,
            // Typ jednotek v textové podobě, například kg (kilogramy), ks (kusů), dg, ml, ... (pro rozevírací nabídku):
            unit: unitDefault,
            // Typ jednotek ve zkratce, tato hodnota je potřeba pouze pro uložení, jinak se nikde nenastavuje apod. (v databázové podobě):
            kindOfUnit: kindOfUnitDefault,
            // True v případě, že pole pro množství Item obsahuje nevalidní hodnotu (záporná čísla), jinak false - validní hodnota:
            itemAmountError: itemAmountErrorDefault,
            // Text pod labelem pro množství položek, bude obsahovat informaci, že se má zadat množství položek (Item) nebo chybu o tom, že se mají zadávat pouze kladná čásla:
            itemAmountErrorHelperText: itemAmountErrorHelperTextDefault,
            // Termín, do kdy by se měla Item splnit:
            deadline: "",
            // Značí, jestli se má label s nastavením termínu splnění položky obarvit jako nevalidní datum nebo ne. True v případě, že je datum starší než aktuální čas za jednu hodinu:
            deadlineError: deadLineErrorDefault,
            // Text pod labelem, který informuje uživatele, že má zadat termín splnění položky nebo že zadal staré datum pro splnění položky (v případě chyby):
            deadlineHelperText: deadlineHelperTextDefault,
            // Priorita položk v textové podobě pro rozevírací nabídku
            priorityTextValue: priorityTextValueDefault,
            // Priorita položky (ve smyslu, jak důležité je splnit položku) v databázové podobě:
            priority: priorityDefault,
            // Položka, kterou chce uživatel upravit (pro uložení ID, nových dat apod.):
            itemForEdit: undefined,
            // Typ nákupní položky - databázová hodnota:
            kindOfShoppingItem: kindOfShoppingItemDefault,
            // Typ nákupní položky - v textové podobě pro zobrazení
            kindOfShoppingItemTextValue: kindOfShoppingItemDefaultTextValue
        };
    }


    // Nastavení dat do formuláře (pro editaci položky item):
    setDataToForm = async item => {
        // Uložení hodnoty do state, pro pozdější uložení do DB (kvůli ID apod.):
        await this.setState({itemForEdit: item});

        let deadline;
        if (item.deadline !== undefined && item.deadline !== null) {
            // Převod data a času (i s časovými zónami) do textu:
            const tempDateTime = item.deadline.toLocaleString();

            // https://medium.com/@hnmtechshack/working-with-moment-js-8393b6dccfcb
            const moment = require('moment');
            // Převod data z textu na požadovaný formát pro datetime picker:
            deadline = moment(tempDateTime).format("YYYY-MM-DDTHH:MM");
        }

        // Nastavení dat do formuláře:
        await this.setState({
            // hodnota v textovém poli pro zadání názvu item:
            valueInTextInputItemName: item.name,
            // true v případě, že pole obsahuje chybnou hodnotu, nebo žádnou apod.:
            // (false, je že neobsahuje chybu, a nebude obarveno pole, ale mělo by být true, proto je kontrola vaidní hodnoty přd přidáním položky)
            errorTextInputItemName: false,
            // text zobrazen pod textovým polem, bude zobrazovat info o nevalidní hodnotě nebo, že se má zadat název Item:
            itemTextInputHelperText: itemTextInputHelperTextDefault,


            // Následující již nejsou hodnoty týkající se textového pole pro název položky:

            // hodnota v textovém poli pro poznámku k item:
            multilineNote: item.note,
            // bude zabalené rozevírací menu s podrobnostmi / další informace o item:
            // expanded: undefined,
            // množství položek, například 5 rohlíků apod.:
            amount: FormItem.getFormValueFromDbValue(item.amount),
            // typ jednotek v textové podobě, například kg (kilogramy), ks (kusů), dg, ml, ...
            unit: getTextValueByKindOfUnitInDbValue(item.kindOfUnit, "value"),
            // Typ jednotek ve zkratce, tato hodnota je potřeba pouze pro uložení, jinak se nikde nenastavuje apod. (v databázové podobě):
            kindOfUnit: item.kindOfUnit,
            // true v případě, že pole pro množství Item obsahuje nevalidní hodnotu (záporná čísla), jinak false - validní hodnota:
            itemAmountError: itemAmountErrorDefault,
            // Text pod labelem pro množství položek, bude obsahovat informaci, že se má zadat množství položek (Item) nebo chybu o tom, že se mají zadávat pouze kladná čásla:
            itemAmountErrorHelperText: itemAmountErrorHelperTextDefault,
            // Termín, do kdy by se měla Item splnit:
            deadline: deadline,
            // Značí, jestli se má label s nastavením termínu splnění položky obarvit jako nevalidní datum nebo ne. True v případě, že je datum starší než aktuální čas za jednu hodinu:
            deadlineError: deadLineErrorDefault,
            // Text pod labelem, který informuje uživatele, že má zadat termín splnění položky nebo že zadal staré datum pro splnění položky (v případě chyby):
            deadlineHelperText: deadlineHelperTextDefault,
            // Priorita položky v textové podobě:
            priorityTextValue: getTextValueByPriorityInDbValue(item.priority),
            // Priorita položky (ve smyslu, jak důležité je splnit položku) (v databázové podobě):
            priority: item.priority,
            // Typ nákupní položky - databázová hodnota:
            kindOfShoppingItem: item.kindOfShoppingItem,
            // Typ nákupní položky - v textové podobě pro zobrazení
            kindOfShoppingItemTextValue: this.getTextValueByKindOfShoppingItemInDbValue(item.kindOfShoppingItem)
        });
    };

    // Získání hodnoty pro množství z databázové hodnoty (pokud je množství null, bude prázdný text, jinak konkrétní číslo):
    static getFormValueFromDbValue = amount => {
        if (amount === null) {
            return "";
        }
        return amount;
    };


    // Nalezení jednotky (konstanty) dle její databázové podoby (kindOfShoppingItem = db_value) a získání její textová podoby pro zobrazení uživateli v rozevírací nabídce:
    getTextValueByKindOfShoppingItemInDbValue = kindOfShoppingItem => {
        // Pokud nebyly zadány jednotky, vrátí se výchozí:
        if (kindOfShoppingItem === undefined || kindOfShoppingItem === null) {
            return kindOfShoppingItemDefault;
        }

        // Nalezení názvu jednotky dle jejího typu (resp. vrátí se unit.value místo unit.db_value):
        const temp = KINDS_OF_SHOPPING_ITEM.find(element => {
            return element.db_value === kindOfShoppingItem;
        });
        return temp.value;
    };


    // Uložení upravené (/ existující) položky ve formuláři:
    saveEditedItem = async () => {
        // (Nastaví se data do upravené položky - kvůli ID apod., poté se uloží do DB):

        // (je třeba vytvořit nový objekt "editItem" (například), kdyby se převzal objekt "const itemForEdit = this.state.itemForEdit" uložil by se příslušný objekt rovnou i do pole, protože obsahuje převzatou referenci)

        // Nastavení dat z formuláře:
        const editItem = {};
        editItem.id = this.state.itemForEdit.id;
        editItem.name = this.state.valueInTextInputItemName;
        editItem.note = this.state.multilineNote;
        // Pokud množství není validní, nastaví se stejně, ale je to jedno, volání funkce se pak zruší:
        editItem.amount = getDbValueForAmount(this.state.amount);
        editItem.kindOfUnit = this.state.kindOfUnit;
        editItem.deadline = this.state.deadline;
        editItem.priority = this.state.priority;
        editItem.kindOfShoppingItem = this.state.kindOfShoppingItem;
        editItem.done = this.state.itemForEdit.done;


        // Je třeba zopakovat text, jestli je hodnota validní (pro případ zpoždění nějakého nastavení hodnoty apod.):

        // Například proto, že při prvním otevření okna je výchozí hodnota false, jako že neobsahuje chybu, ale pole je prázdné, což je špatně, proto, se zde musí opravdu otestovat ještě jednou, aby neprošla například tato prázdná "úvodní" hodnota:
        if (!this.isItemNameValidForEdit(this.state.valueInTextInputItemName, editItem.id)) {
            return;
        }


        // V případě, že je zadáno množství, otestuje se, jestli se jedná o kladné celé číslo:
        if (!this.isAmountValid(this.state.amount)) {
            // Otevření menu s podrobnosti Item v případě, že není (aby si uživatel všiml chyby apod.):
            await this.setState({expanded: "exp"});
            return;
        }


        // V případě, že je zadáno datum, které je starší než aktuální čas za 1 hodinu, nebude čas akceptován:
        if (this.state.deadline !== "" && !this.isDateValid(this.state.deadline)) {
            return;
        }


        // Aktualizace položky:
        await this.props.updateItem(editItem);

        // Zrušení editace položky, výše byly uloženy změny, zde se "vyprázdní formulář" a nastaví správná tlačítka pro práci s formulářem:
        await this.cancelItemEditing();
    };


    // Zrušení editace položky, tzn. data ve formuláči se vyprázdní (nastaví se výchozí hodnoty):
    cancelItemEditing = async () => {
        // Aby nedošlo k uložení položky a změnily se data / komponenty ve formuláři na přidání položky:
        await this.setState({itemForEdit: undefined});
        // Nastavení výchozí hodnoty do textového pole:
        await this.setDefaultValuesToItemNameTextField();
        // Nastavení výchozích hodnot pro podrobností položky jako je datum, poznámka, množství atd.
        await this.setDefaultValuesToItemDetails();
    };


    // Nastaví se hodnota expanded na false, když se má zabalit, na exp, když se má menu rozbalit
    handleExpandChange = panel => async (event, expanded) => {
        await this.setState({expanded: expanded ? panel : false});
    };


    // Uložení hodnoty v event do proměnné s názvem "name":
    handleChange = name => async event => {
        // V případě, že se nastavují jednotky, uloží se / nastaví se i text té jednotky pro práci s DB:
        if (name === "unit") {
            // Nalezené jednotky podle její textové podoby:
            const temp = UNITS.find(element => {
                return element.value === event.target.value;
            });
            // Uložení / nastavení zkratky v databázové podobě:
            await this.setState({kindOfUnit: temp.db_value});
        } else if (name === "priorityTextValue") {
            // Nalezené jednotky podle její textové podoby:
            const temp = PRIORITIES.find(element => {
                return element.value === event.target.value;
            });
            await this.setState({priority: temp.db_value});
        } else if (name === "kindOfShoppingItemTextValue") {
            // Nalezené jednotky podle její textové podoby:
            const temp = KINDS_OF_SHOPPING_ITEM.find(element => {
                return element.value === event.target.value;
            });
            await this.setState({kindOfShoppingItem: temp.db_value});
        }
        await this.setState({[name]: event.target.value});
    };


    // Nastavení konečného termínu splnění úlohy a kontrola, jestli je datum validní:
    setDeadLine = async event => {
        const deadline = event.target.value;
        await this.setState({deadline: deadline});
        await this.isDateValid(deadline);
    };


    // Kontrola, jestli se jedná o validní datum, které je starší než aktuální datum za jednu hodinu:
    isDateValid = async date => {
        // Získání času za jednu hodinu:
        let dateInOneHour = new Date();
        dateInOneHour.setHours(dateInOneHour.getHours() + 1);

        // Pokud je zadaný čas starší než aktuální čas za jednu hodinu, nebude akceptován:
        if (new Date(date) <= dateInOneHour) {
            await this.setState({deadlineError: true});
            await this.setState({deadlineHelperText: "Deadline must be older than current time in 1 hour"});
            return false;
        }

        await this.setState({deadlineError: false});
        await this.setState({deadlineHelperText: deadlineHelperTextDefault});
        return true;
    };


    // Nastavení atributu state na hodnotu value:
    setStateValue = async (state, value) => {
        await this.setState({[state]: value});
    };


    // bylo potřeba přepsat funkci "hlídající" klávesu Enter:
    _handleKeyPress = async e => {
        if (e.key === 'Enter') {
            // Na stisknutí klávesy Enter se zavolá funkce, která otestuje zadaná data a případně se přidá / vytvoří nová položka:
            await this.addItem();
        }
    };


    // Zavolání funkce pro přidání položky z předka a předání parametrů (pouze v přpadě, že textové pole obsauje validní hodnotu):
    addItem = async () => {
        // Je třeba zopakovat text, jestli je hodnota validní (pro případ zpoždění nějakého nastavení hodnoty apod.):

        // Například proto, že při prvním otevření okna je výchozí hodnota false, jako že neobsahuje chybu, ale pole je prázdné, což je špatně, proto, se zde musí opravdu otestovat ještě jednou, aby neprošla například tato prázdná "úvodní" hodnota:
        if (!await this.isItemNameValid(this.state.valueInTextInputItemName)) {
            return;
        }

        // V případě, že je zadáno množství, otestuje se, jestli se jedná o kladné celé číslo:
        if (!await this.isAmountValid(this.state.amount)) {
            // Otevření menu s podrobnosti Item v případě, že není (aby si uživatel všiml chyby apod.):
            await this.setState({expanded: "exp"});
            return;
        }

        // V případě, že je zadáno datum, které je starší než aktuální čas za 1 hodinu, nebude čas akceptován:
        if (this.state.deadline !== "" && !await this.isDateValid(this.state.deadline)) {
            return;
        }

        // Zavolání přidání položky v rodiči:
        if (typeof this.props.addItem === "function") {
            const result = await this.props.addItem(this.state);
            // Nastaví se výchozí hodnoty pouze v případě, že se položka úspěšně vytvořila:
            if (result) {
                // Nastavení výchozí hodnoty do textového pole:
                await this.setDefaultValuesToItemNameTextField();
                // Nastavení výchozích hodnot pro podrobností položky jako je datum, poznámka, množství atd.
                await this.setDefaultValuesToItemDetails();
            }
        }
    };


    // Nastavení výchozích hodnot pro textové pole pro název Item ve formuláři:
    setDefaultValuesToItemNameTextField = async () => {
        // hodnota v textovém poli pro zadání názvu item:
        await this.setState({valueInTextInputItemName: ""});
        // true v případě, že pole obsahuje chybnou hodnotu, nebo žádnou apod.:
        await this.setState({errorTextInputItemName: true});
        // text zobrazen pod textovým polem, bude zobrazovat info o nevalidní hodnotě nebo, že se má zadat název Item:
        await this.setState({itemTextInputHelperText: itemTextInputHelperTextDefault});
    };


    // Nastavení výchozích hodnot do polí pro podrobnosti položky (poznámka, množství, ...):
    setDefaultValuesToItemDetails = async () => {
        // hodnota v textovém poli pro poznámku k item:
        await this.setState({multilineNote: multilineNoteDefault});
        // bude zabalené rozevírací menu s podrobnostmi / další informace o item:
        await this.setState({expanded: undefined});
        // množství položek, například 5 rohlíků apod.:
        await this.setState({amount: amountDefault});
        // typ jednotek v textové podobě, například kg (kilogramy), ks (kusů), dg, ml, ...
        await this.setState({unit: unitDefault});
        // Typ jednotek ve zkratce, tato hodnota je potřeba pouze pro uložení, jinak se nikde nenastavuje apod. (v databázové podobě):
        await this.setState({kindOfUnit: kindOfUnitDefault});
        // true v případě, že pole pro množství Item obsahuje nevalidní hodnotu (záporná čísla), jinak false - validní hodnota:
        await this.setState({itemAmountError: itemAmountErrorDefault});
        // Text pod labelem pro množství položek, bude obsahovat informaci, že se má zadat množství položek (Item) nebo chybu o tom, že se mají zadávat pouze kladná čásla:
        await this.setState({itemAmountErrorHelperText: itemAmountErrorHelperTextDefault});
        // Termín, do kdy by se měla Item splnit:
        await this.setState({deadline: ""});
        // Značí, jestli se má label s nastavením termínu splnění položky obarvit jako nevalidní datum nebo ne. True v případě, že je datum starší než aktuální čas za jednu hodinu:
        await this.setState({deadlineError: deadLineErrorDefault});
        // Text pod labelem, který informuje uživatele, že má zadat termín splnění položky nebo že zadal staré datum pro splnění položky (v případě chyby):
        await this.setState({deadlineHelperText: deadlineHelperTextDefault});
        // Priorita položky v textové podobě:
        await this.setState({priorityTextValue: priorityTextValueDefault});
        // Priorita položky (ve smyslu, jak důležité je splnit položku) (v databázové podobě):
        await this.setState({priority: priorityDefault});
        // Typ nákupní položky - databázová hodnota:
        await this.setState({kindOfShoppingItem: kindOfShoppingItemDefault});
        // Typ nákupní položky - v textové podobě pro zobrazení
        await this.setState({kindOfShoppingItemTextValue: kindOfShoppingItemDefaultTextValue});
    };


    // Funkce se zavolá po potvrzení formuláře klávesou enter, nejprve za předejde refresh stránky a poté se zavolá funkce pro přidání nové položky:
    handleSubmit = async event => {
        event.preventDefault();
        await this.addItem();
    };


    // return true - text obsahuje pouze bílé znaky, jinak false:
    static containsTextOnlyWhiteSpace = text => {
        return !text.replace(/\s/g, '').length;
    };


    // Nastavení hodnot v textovém poli pro název Item tak, aby uživateli oznámil, že obsahuje chybu - nevalidní hodnota:
    setErrorValues = async helperText => {
        // Nastavení itemTextInputHelperText, aby oznámila chybu uživateli (text pod textovým polem):
        await this.setStateValue("itemTextInputHelperText", helperText);
        // nastavení obarvení textového pole, jako že obsahuje chybu:
        await this.setStateValue("errorTextInputItemName", true);
    };


    // Kontrola, zdali je název Item validní - vrátí se true, jinak false, když není text validní apod.:
    isItemNameValid = async name => {
        if (name === "") {
            // Nastavené chybové zprávy pod textovým polem a chybového obarvení textového pole:
            await this.setErrorValues("Field is empty");
            return false;
        }

        // Test, jestli nejsou zadány pouze bílé znaky:
        else if (FormItem.containsTextOnlyWhiteSpace(name)) {
            // Nastavené chybové zprávy pod textovým polem a chybového obarvení textového pole:
            await this.setErrorValues("Text contains only white space !");
            return false;
        }

        // Test, jestli item s názvem tempText ještě neexistuje:
        else if (this.props.existItem(name)) {
            // Nastavené chybové zprávy pod textovým polem a chybového obarvení textového pole:
            await this.setErrorValues("Item with name '" + name + "' already exist !");
            return false;
        }

        // Nastavení itemTextInputHelperText, aby věděl, že text je v pohodě - validní (text pod textovým polem):
        await this.setStateValue("itemTextInputHelperText", itemTextInputHelperTextDefault);
        // Textové pole bude obarveno "normálně" - validní text:
        await this.setStateValue("errorTextInputItemName", false);
        return true;
    };


    // Kontrola, zdali je název položky validní - vrátí se true, jinak false, když není text validní apod.:
    // Pro kontrolu existence položky je třeba vynechat položku se stejným ID, protože se jedná kontrolu hodnot již existující položky.
    isItemNameValidForEdit = async (name, id) => {
        if (name === "") {
            // Nastavené chybové zprávy pod textovým polem a chybového obarvení textového pole:
            await this.setErrorValues("Field is empty");
            return false;
        }

        // Test, jestli nejsou zadány pouze bílé znaky:
        else if (FormItem.containsTextOnlyWhiteSpace(name)) {
            // Nastavené chybové zprávy pod textovým polem a chybového obarvení textového pole:
            await this.setErrorValues("Text contains only white space !");
            return false;
        }

        // Test, jestli item s názvem tempText ještě neexistuje:
        else if (this.props.checkExistItem(name, id)) {
            // Nastavené chybové zprávy pod textovým polem a chybového obarvení textového pole:
            await this.setErrorValues("Item with name '" + name + "' already exist !");
            return false;
        }

        // Nastavení itemTextInputHelperText, aby věděl, že text je v pohodě - validní (text pod textovým polem):
        await this.setStateValue("itemTextInputHelperText", itemTextInputHelperTextDefault);
        // Textové pole bude obarveno "normálně" - validní text:
        await this.setStateValue("errorTextInputItemName", false);
        return true;
    };


    // Kontrola validního vstupu v poli pro název Item, vráti se true v případě, že jsou hodnoty validní, jinak false:
    checkValueInItemNameTextInput = async event => {
        const tempText = event.target.value;
        await this.setState({valueInTextInputItemName: tempText});

        // Nastavení hodnot tak, aby ukázali uživateli, jestli je název Item validní nebo ne:
        // (Je třeba rozlišovat, zdali se má kontrolovat název všech položek, nebo všech kromě té, která se právě upravuje ve formuláři):
        if (this.state.itemForEdit === undefined)
            await this.isItemNameValid(tempText);
        else
            await this.isItemNameValidForEdit(tempText, this.state.itemForEdit.id);
    };


    // "Uložení" hodnoty o nastaveném množství a kontrola, jestli se nejedná o záporné množství:
    handleAmountTextField = async event => {
        const amount = event.target.value;
        await this.setState({amount: amount});
        await this.isAmountValid(amount);
    };


    // V případě, že je amount menší než nula, nastaví se k labelu pro zadání množství chybové oznámení, že lze zadat pouze kladné hodnoty, jinak se label "obarví" jako, že se jedná o validní hodnotu:
    isAmountValid = async amount => {
        if (amount === "") {
            // Pro nezadané číslo se várít true - nejedná se o chybu, číslo se pouze nezobrazí v okně:
            return true;
        }

        if (amount >= 0 && (amount.toString().match(/^\d+\.\d+$/) !== null || amount.toString().match(/^\d+$/) !== null)) {
            // číslo je kladné a odpovídá regulárnímu výrazu pro celé nebo desetinné číslo:
            await this.setState({itemAmountError: false});
            await this.setState({itemAmountErrorHelperText: itemAmountErrorHelperTextDefault});
            // číslo je validní (/ v pořádku):
            return true;
        }

        // Zde je zadané datum nevalidní, tedy starší než aktuální + jedna hodina. "Obarví" se label na červeno a zobrazí se chybové hlášení pod labelem:
        await this.setState({itemAmountError: true});
        await this.setState({itemAmountErrorHelperText: "Only positive numbers (tenths with '.' or ',')"});
        return false;
    };


    // Získání responzivní šířky komponenty:
    getResponsiveWidth = () => {
        const size = this.props.responsiveWidth;
        let responsiveSize = 290;

        if (size <= 600)
            responsiveSize = 280;

        if (size <= 380)
            responsiveSize = 190;

        return responsiveSize;
    };


    render() {
        const responsiveWidth = this.getResponsiveWidth();

        // Tlačítko pro vytvoření nové položky nebo pro uložení upravené položky:
        let addOrEditItem;
        // Pokud byla do formuláře nastavena položka pro editaci, ... nastavení tlačítek:
        if (this.state.itemForEdit !== undefined) {
            addOrEditItem = <span>
                {/*Tlačítko pro uložení upravené položky:*/}
                <ImageButton onClick={this.saveEditedItem} image={EditIcon} color={"inherit"} alt={"Save todo item"}
                             tooltip={"Save"}/>
                {/*Tlačítko pro zrušení editace položky (vyprázdní formulář a proměnné):*/}
                <ImageButton onClick={this.cancelItemEditing} image={CancelEditingIcon} color={"secondary"}
                             alt={"Cancel item editing"}
                             tooltip={"Cancel"}/>
            </span>
        } else {
            // Tlačítko pro přidání nové položky:
            addOrEditItem =
                <ImageButton onClick={this.addItem} image={AddItemIcon} color={"inherit"} alt={"Add todo item"}
                             tooltip={"Add"}/>
        }


        return (
            <form onSubmit={this.handleSubmit} className="Container" noValidate
                  autoComplete="off">
                {/*Pole pro zadání názvu položky:*/}
                <TextField
                    id="itemName"
                    label="Item"
                    value={this.state.valueInTextInputItemName}// výchozí hodnota
                    placeholder="Item name"// výchozí text
                    helperText={this.state.itemTextInputHelperText}
                    fullWidth={true}// šířka přes celé okno
                    autoFocus={true}// při spuštění se na toto pole nastaví kurzor
                    margin="normal"
                    variant="filled"// šedivé poličko, další: standard, outlined
                    required={true}
                    error={this.state.errorTextInputItemName}// obarvení "chybový" stav
                    InputLabelProps={{shrink: true}}
                    onKeyPress={this._handleKeyPress}
                    onChange={this.checkValueInItemNameTextInput}
                    className="TextFieldItemName"
                />

                {/*Tlačítko pro vytvoření nové položky nebo pro uložení upravené (/ existující) položky:*/}
                {addOrEditItem}

                {/*Tlačítko pro vymazání textu v poli:*/}
                <ImageButton onClick={this.setDefaultValuesToItemNameTextField} color={"secondary"}
                             image={BackspaceIcon} alt={"Clear input text field"} tooltip={"Clear"}/>

                {/*Tlačítko pro vymazání veškerých vytvořených položek:*/}
                <ImageButton onClick={this.props.deleteAllItems} color={"secondary"} image={DeleteAllItemsIcon}
                             alt={"Delete all items"} tooltip={"Delete all items in list"}/>


                {/*Rozbalovací nabídka s dalšími parametry nastavení:*/}
                <div className="RootDetails">
                    <ExpansionPanel expanded={this.state.expanded === "exp"}
                                    onChange={this.handleExpandChange("exp")}>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                            <div className="ItemDetailHeading">Details</div>
                        </ExpansionPanelSummary>


                        {/*Volitelná poznámka k položce:*/}
                        <TextField
                            id="itemNote"
                            label="Note"
                            multiline
                            rowsMax="4"
                            value={this.state.multilineNote}
                            placeholder={this.state.multilineNote}
                            onChange={this.handleChange("multilineNote")}
                            className="TextFieldNote"
                            margin="normal"
                            variant="filled"
                            fullWidth={false}
                        />


                        <br/>


                        {/*Textové pole pro zadání množství:*/}
                        <TextField
                            id="itemAmount"
                            label="Amount"
                            value={this.state.amount}
                            helperText={this.state.itemAmountErrorHelperText}
                            error={this.state.itemAmountError}
                            placeholder="0"
                            onChange={this.handleAmountTextField}
                            type="number"
                            className="TextFieldNumber"
                            InputProps={{inputProps: {min: 0}}}// Aby nešlo zadat záporná čísla
                            InputLabelProps={{shrink: true}}
                            margin="normal"
                            variant="filled"
                        />


                        {/*Textové pole pro zadání "typu" množství položek, například rohlík 5 kusů, salát 6 kg apod. jedná se o zadání jednotek:*/}
                        <SelectTextField label={"Unit"} value={this.state.unit}
                                         handleChange={this.handleChange("unit")} helperText={"Kind of amount"}
                                         items={UNITS} width={80}/>


                        <br/>


                        {/*Nastavení konečného termínu "do kdy" by se měla úloha vykonat:*/}
                        <TextField
                            id="itemDeadline"
                            label="Deadline"
                            type="datetime-local"
                            value={this.state.deadline}
                            onChange={this.setDeadLine}
                            error={this.state.deadlineError}
                            helperText={this.state.deadlineHelperText}
                            className="TextFieldDateTime"
                            InputLabelProps={{shrink: true}}
                            variant="filled"
                        />


                        <br/>


                        {/*Nastavení priority (/ kategorie):*/}
                        <SelectTextField label={"Priority"} value={this.state.priorityTextValue}
                                         handleChange={this.handleChange("priorityTextValue")}
                                         helperText={"Item priority"}
                                         items={PRIORITIES} width={responsiveWidth}/>


                        {/*Nastavení typu položky:*/}
                        <SelectTextField value={this.state.kindOfShoppingItemTextValue}
                                         handleChange={this.handleChange("kindOfShoppingItemTextValue")}
                                         helperText={"Kind of shopping item"}
                                         items={KINDS_OF_SHOPPING_ITEM} width={responsiveWidth}/>
                    </ExpansionPanel>
                </div>
            </form>
        );
    }
}