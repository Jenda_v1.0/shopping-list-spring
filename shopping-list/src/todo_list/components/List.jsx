import * as React from "react";
import FormItem from "./FormItem";
import TabsItem from './TabsItem';
import Calls from '../server/calls';
import FilterForm from './FilterForm';
import {FILTER_BY} from '../constants/FilterBy';
import Loading from "./Loading";
import * as LOADING from '../constants/Loading';
import {getDbValueForAmount} from "../constants/Helper";
import SnackbarMessage from "./SnackbarMessage";
import './styles/List.css';
import ListOverview from "./list_overview/ListOverview";

/**
 * Komponenta obsahující formulář pro vytvoření nové položky a pod ním TabsItem, což jsou záložky filtrující jednotlivé položky, které by měl uživatel vykonat (které uživatel vytvořil). (Komponenta TabsItem je vykreslena zde, aby se ušetřilo předávání hodnot.)
 *
 * Tato komponenta obsahuje pole items obsahující vytvořené položky, dále CRUD operace nad nimi.
 */

export default class List extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // Nákupní listy v DB:
            lists: [],
            // Uživatelem označený list, jehož položky se mají zobrazit:
            // (je zde potřeba, aby například při vytvoření položky jí bylo možné přiřadit list, podobně při editaci, protože položka si kvůli serializaci nenačte příslušný list)
            selectedList: undefined,
            // Nákupní položky v označeném nákupním listu:
            items: [],
            // Hodnota v textovém poli pro filtrování položek:
            filterValue: "",
            // Hodnota zvolená v picker pro typ hodnoty, dle které se mají filtrovat položky:
            filterBy: FILTER_BY[0].value,
            // Fáze načítání dat:
            loading: LOADING.LOADING,
            // ("Zdali se má zobrazit načítací animace místo záložek s nákupními položkami")
            // Zdali se v části se záložkami s položkami má zobrazit animace pro načítání (true) nebo ne (false),
            // true v případě, že se načítají položky z DB, jinak false,
            // je to potřeba pro to, aby se změnila pouze ta jedna část, jinak by se zbytečně překreslovala celá stránka, navíc by se neoznačil příslušný listt:
            loadingItemsFromList: false,
            // Velikost některých komponent, kterým je třeba nastavit respozivni velikost:
            responsiveWidth: 0,
            // Položka, kterou chce uživatel upravit (pro natavení hodnot do formuláře):
            itemForEdit: undefined,
            // Nastavení varovné zprávy:
            snackInfo: {
                variant: "error",
                open: false,
                message: "Unknown error."
            }
        };

        // Reference na komponentu pro zavolání metody z předka do potomka (pro nastavení hodnot ve formuláři):
        // https://stackoverflow.com/questions/37949981/call-child-method-from-parent
        this.formItemRef = React.createRef();
    }


    // Nastavení označeného listu a zobrazení jeho položek:
    selectList = async list => {
        await this.setState({selectedList: list});
        await this.loadItems(list.id);
    };


    // Získání listu s listId (vyhledávání pouze z načtených listů):
    getListById = listId => {
        return this.state.lists.find(list => {
            return list.id === listId;
        })
    };


    // Vytvoření nového nákupního listu:
    createShoppingList = async name => {
        // Zobrazení načítání místo položek:
        await this.setLoadingState(LOADING.LOADING);

        // Nový objekt pro list (pro zaslání na kontrolér):
        const newList = {
            name: name
        };

        // Zaslání požadavku pro vytvoření nového nákupního listu:
        await Calls.createShoppingList(newList)
            .then(async response => {
                // Aby se nemusely načítat všechny nové listy z DB, přidá se nově vytvořený list:
                const lists = [...this.state.lists];
                lists.push(response.data);
                await this.setState({lists: lists});
            })
            .catch(async error => {
                await this.errorCallback(error);
            });

        await this.setLoadingState(LOADING.DONE);
    };


    // Aktualizace nákupního listu:
    updateShoppingList = async list => {
        // Zobrazení načítání místo položek:
        await this.setLoadingState(LOADING.LOADING);

        // Zaslání požadavku pro vytvoření nového nákupního listu:
        await Calls.updateShoppingList(list)
            .then(async response => {
                // Načtení aktuálního listu z response (stejný jako v parametru list):
                const updatedList = response.data;
                // Aktualizace hodnoty v state:
                this.setState({
                    lists: this.state.lists.map(list => (list.id === updatedList.id ? Object.assign({}, list, {updatedList}) : list))
                });
            })
            .catch(async error => {
                await this.errorCallback(error);
            });

        await this.setLoadingState(LOADING.DONE);
    };


    // Zjištění, zdali je název listu již využit nebo ne:
    isListNameInUse = async name => {
        return await Calls.isShoppingListNameInUse(name)
            .then(async response => {
                return response.data;
            })
            .catch(async error => {
                await this.errorCallback(error);
                // Vrátí se true, jako že je název listu již využit - výchozí:
                return true;
            });
    };


    // Zjištění, zdali je název listu již využit nebo ne, ale parametr "list" je celý objekt, z testování názvů se vynechá tento existující list:
    isListNameInUseExceptExisting = async list => {
        return await Calls.isShoppingListNameInUseExcept(list)
            .then(async response => {
                return response.data;
            })
            .catch(async error => {
                await this.errorCallback(error);
                // Vrátí se true, jako že je název listu již využit - výchozí:
                return true;
            });
    };


    // Vymazání konkrétního nákupního listu s id listId:
    deleteShoppingList = async listId => {
        // Dotaz na potvrzení smazání položky:
        const listForDeleteName = this.getListById(listId).name;
        if (!window.confirm("Are you sure you want to delete the list '" + listForDeleteName + "' ?"))
            return;

        // Aby nebylo třeba zasílat nový požadavek, list se vymaže z uloženého pole:
        let tmpLists = [...this.state.lists];
        tmpLists = tmpLists.filter(list => {
            return list.id !== listId;
        });
        await this.setState({lists: tmpLists});

        // Zobrazení načítání místo položek:
        await this.setState({loadingItemsFromList: true});

        // Zaslání požadavku na smazání z DB:
        await Calls.deleteShoppingList(listId)
            .then(async () => {
                const {selectedList} = this.state;
                // V případě, že se smazal list, jehož položky jsou právě zobrazeny, je třeba smazat i ty aktuálně zobrazované položky:
                if (selectedList !== undefined && selectedList.id === listId) {
                    await this.setState({selectedList: undefined});
                    await this.loadItems(listId);
                }
            })
            .catch(async error => {
                await this.errorCallback(error);
            });

        await this.setState({loadingItemsFromList: false});
    };


    // Vymazání veškerých nákupních listů z DB:
    deleteAllShoppingLists = async () => {
        if (!window.confirm("Are you sure you want to delete all shopping lists ?"))
            return;

        // Zobrazení načítání místo položek:
        await this.setLoadingState(LOADING.LOADING);

        // Zaslání požadavku na smazání z DB:
        await Calls.deleteAllShoppingLists()
            .then(async () => {
                // Zde byly úspěšně vymazány položky z DB:
                await this.setState({lists: []});
                await this.setState({items: []});
            })
            .catch(async error => {
                await this.errorCallback(error);
            });

        await this.setLoadingState(LOADING.DONE);
    };


    // Zjištění, zdali je list s id listId označen nebo ne (kvůli označení příslušného chcb):
    isListWithIdSelected = listId => {
        const {selectedList} = this.state;

        if (selectedList === undefined)
            return false;
        return selectedList.id === listId;
    };


    // Nastavení fáze pro znázornění animace s načítáním:
    setLoadingState = async state => {
        await this.setState({loading: state});
    };


    // Nastavení aktuální šířky okna:
    updateWindowWidth = async () => {
        await this.setState({responsiveWidth: window.innerWidth});
    };


    // Výchozí načtení hodnot z DB:
    async componentDidMount() {
        // Toto je třeba nastavit pro přechod na novou verzi:
        // https://material-ui.com/style/typography/#migration-to-typography-v2
        window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;


        // Přidání události na změnu velikosti okna obrazovky:
        // https://stackoverflow.com/questions/19014250/rerender-view-on-browser-resize-with-react
        window.addEventListener("resize", this.updateWindowWidth);
        // Zrovna se nastaví aktuální velikost pro výchozí zobrazení komponent:
        await this.updateWindowWidth();

        await this.loadAllLists();

        await this.setState({loading: LOADING.DONE});
    }


    // Načtení veškerých listů v DB:
    loadAllLists = async () => {
        await Calls.getShoppingLists()
            .then(async response => {
                await this.setState({lists: response.data});
            })
            .catch(async error => {
                await this.errorCallback(error);
            });
    };


    // Zobrazení hlášky uživateli:
    showError = message => {
        console.log(message);
        alert(message);
    };


    // Načtení veškerých nákupních položek patřících k listu s id listId:
    loadItems = async listId => {
        if (listId <= 0) {
            // Například není označen žádný list:
            await this.setState({items: []});
            return;
        }

        // (Zde nelze nastavit dialog pro načítání, protože by se překreslila stránka a nebyl by pak označen potřebný chcb s označeným listem)
        // Když se mají načíst položky, zobrazí se místo nich animace reprezentující načítání (dokud se položkynačítají - než se vyřídí požadavek):
        await this.setState({loadingItemsFromList: true});

        await Calls.getItemsByShoppingListId(listId)
            .then(async response => {
                await this.setState({items: response.data});
            })
            .catch(async error => {
                await this.errorCallback(error);
            });

        await this.setState({loadingItemsFromList: false});
    };


    // Zobrazení dialogu (ve spodní části okna) s přehledem nastalé chyby (v parametru error):
    errorCallback = async error => {
        if (error.response === undefined) {
            // Zobrazení dialogu:
            await this.openSnackbarWithVariantAndFormattedText("error", "There was an exception.");
            return;
        }

        const {timeStamp, message, fieldErrors} = error.response.data;

        const moment = require('moment');
        const timeStampFormatted = moment(timeStamp).format("DD.MM.YYYY HH:MM:SS");

        let messageInfo = "At " + timeStampFormatted + ", there was an error: " + message;

        // Pokud fieldErrors existuje a obsahuje alespoň jednu položku (chybovou zprávu o validaci nějaké hodnoty):
        if (fieldErrors.length && fieldErrors.length > 0) {
            fieldErrors.forEach(error => {
                messageInfo += "\n- " + error;
            })
        }

        console.log("Exception: " + messageInfo);

        // Zobrazení zprávy se stylyzovaným spanem (aby zobrazoval text i na nových řádcích):
        await this.openSnackbarWithVariantAndFormattedText("error", messageInfo);
    };


    // Získání položky s id z pole items:
    getItemById = id => {
        return this.state.items.find(function (item) {
            return item.id === id;
        });
    };


    // Odstranění položky s id v poli items (pole všech položek):
    deleteItem = async id => {
        // Nalezení jedné konkrétní položky s konkrétním ID:
        const item = this.getItemById(id);

        // Podmínka by neměla být nikdy splněna:
        if (item === undefined) {
            const error = "Item with id: '" + id + "' was not found !";
            this.showError(error);
            return;
        }

        // Dotaz na potvrzení smazání položky:
        if (!window.confirm("Are you sure you want to delete the item '" + item.name + "' ?"))
            return;

        // Smazání položky z DB a z pole pro zobrazení:
        await Calls.deleteShoppingItem(id)
            .then(async () => {
                // Z pole se vymaže položka s příslušným id:
                let items = [...this.state.items];
                items = items.filter(item => {
                    return item.id !== id;
                });
                await this.setState({items: items});
            })
            .catch(async error => {
                await this.errorCallback(error);
            });
    };


    // Odstranění veškerých položek v listu:
    deleteAllItems = async () => {
        const {selectedList} = this.state;
        // Zdali je označen list, jehož položky se mají smazat:
        if (selectedList === undefined) {
            await this.openSnackbarWithVariantAndFormattedText("error", "A list whose items are to be deleted is not selected.");
            return;
        }

        // Položky má smysl smazat pouze v případě, že alespoň jedna existuje, jina nemá smysl se ani ptát apod.:
        if (this.state.items.length === 0)
            return;

        // Dotaz na potvrzení, zdali se mají smazat veškeré položky v DB:
        if (!window.confirm("Are you sure you want to delete all items in selected list ?"))
            return;

        await this.setLoadingState(LOADING.LOADING);

        // Zavolání metody pro vymazání tabulky s položkami:
        await Calls.deleteAllItemsByListId(selectedList.id)
            .then(async () => {
                await this.setState({items: []});
            })
            .catch(async error => {
                await this.errorCallback(error);
            });

        await this.setLoadingState(LOADING.DONE);
    };


    // Nastavení, zdali je Item splněna nebo ne:
    changeDoneStatusItem = async id => {
        // Získání položky z pole:
        const updateItem = this.getItemById(id);
        // Nastavení splnění položky:
        updateItem.done = !updateItem.done;

        // Aktualizace položky:
        await this.updateItem(updateItem);
    };


    // Aktualizace položky v poli a v DB:
    updateItem = async editedItem => {
        const {selectedList} = this.state;
        if (selectedList === undefined) {
            await this.openSnackbarWithVariantAndFormattedText("warning", "The list where the item is to be added moved is not marked.");
            return;
        }

        // Nastavení listu příslušné položce, aby bylo možné ji aktualizovat (list musí být vždy přítomen):
        editedItem.list = this.state.selectedList;

        // Aktualizace položky v DB:
        const updatedItemInDb = await this.updateItemInDB(editedItem);

        // Pokud se položka vrátila, je možné ji aktualizovat i v poli pro zobrazení uživateli (tj. položka se úspěšně aktualizovala v DB):
        // (pokud se vrátí null, nastala výjimka)
        if (updatedItemInDb !== null) {
            // Aktualizace položky v poli (aby se nemuseli přenačítat veškerá data v DB):
            await this.setState({
                items: this.state.items.map(item => (item.id === editedItem.id ? Object.assign({}, item, editedItem) : item))
            });
        }
    };


    // Aktualizace položky v DB:
    updateItemInDB = async item => {
        return await Calls.updateShoppingItem(item)
            .then(async response => {
                return response.data;
            })
            .catch(async error => {
                await this.errorCallback(error);
                return null;
            });
    };


    // Vytvoření nové položky:
    addItem = async formState => {
        const {selectedList} = this.state;

        if (selectedList === undefined) {
            await this.openSnackbarWithVariantAndFormattedText("warning", "The list where the item is to be added is not marked.");
            return false;
        }

        // vytvoření nové položky s nastavenými / výchozími hodnotami:
        let newItem = {
            done: false,// výchozí hodnota je false, tj. při vytvoření položky nebude splněna
            name: formState.valueInTextInputItemName,
            note: formState.multilineNote,
            // Získání hodnoty množství pro vložení do DB (buď konkrétní zadané číslo nebo null hodnota, když nebude číslo zadané):
            amount: getDbValueForAmount(formState.amount),
            kindOfUnit: formState.kindOfUnit,
            deadline: formState.deadline,
            priority: formState.priority,
            kindOfShoppingItem: formState.kindOfShoppingItem,
            list: selectedList
        };


        // (Když se má přidat nová položka, místo záložek s již existujícímí položkami se zobrazí ikona animace načítání, aby uživatel na jinou neklikl, věděl, že se vytváří položka apod.)
        // (navíc, je vhodné, když se zobrazí to načítání pouze přes ty položky a na přes celou stránku, pak se odstraní data při překreslení)
        await this.setState({loadingItemsFromList: true});

        let errorOccurred = false;
        // Nová položka se vloží do DB a vrátí se s vygenerovaným ID:
        await Calls.createShoppingItem(newItem)
            .then(async response => {
                // Aby se nemusely načítat všechny nové položky z DB, aktualizuje se pouze ta jedna:
                const items = [...this.state.items];
                items.push(response.data);
                await this.setState({items: items});
            })
            .catch(async error => {
                errorOccurred = true;
                await this.errorCallback(error);
            });

        await this.setState({loadingItemsFromList: false});

        // Pokud nastala chyba vrátí se false, jinak true, že se položka úspěšně vytvořila:
        return !errorOccurred;
    };


    // Nastavení hodnoty val do atributu key objektu cat:
    setValueToObject = async (cat, key, val) => {
        const category = {...this.state[cat]};
        category[key] = val;
        await this.setState({[cat]: category});
    };


    // Otevření dialogu pro zobrazení nějaké zprávy (/ chyby) uživateli, dialog bude typu "variant" a bude zobrazovat zformátovanou zprávu "message":
    openSnackbarWithVariantAndFormattedText = async (variant, message) => {
        await this.openSnackbar(variant, <span id={"errorMessageInfo"}>{message}</span>);
    };


    // Otevření objektu snackInfo (dialogu) a nastavení varianty a hlášky:
    openSnackbar = async (variant, message) => {
        await this.setValueToObject("snackInfo", "open", true);
        await this.setValueToObject("snackInfo", "variant", variant);
        await this.setValueToObject("snackInfo", "message", message);
    };


    // Nastavení objektu snackInfo tak, aby se zavřelo příslušné okno:
    handleCloseSnackbar = async () => {
        await this.setValueToObject("snackInfo", "open", false);
    };


    // Nastavení položky s id pro editaci, tj. že se nastaví hodnoty položky do formuláře:
    setItemForEdit = async id => {
        let itemById = this.getItemById(id);
        await this.setState({itemForEdit: itemById});

        // Nastavení dat do formuláře:
        await this.formItemRef.current.setDataToForm(itemById);
    };


    // Zjištění, jestli již položka s textem v parametru "text" existuje nebo ne:
    existItem = text => {
        return this.state.items.filter((item) => item.name === text).length > 0;
    };


    // Zjištění, jestli již položka s textem v parametru "text" existuje nebo ne:
    // Ale vynechá se testování položky s příslušným id:
    checkExistItem = (text, id) => {
        return this.state.items.filter((item) => item.name === text && item.id !== id).length > 0;
    };


    // Nastavení atributu na hodnotu, která se má vyhledávat nebo "dle které hodnoty" se má vyhledávat v položkách:
    setAndFilterValues = name => async event => {
        await this.setState({[name]: event.target.value});
    };


    // Získání komponent, které se mají vykreslit:
    getComponentsForRender = () => {
        switch (this.state.loading) {
            case LOADING.LOADING:
                return <React.Fragment>
                    {/*Na umístění komponenty nezáleží, zpráva se zobrazí vždy vlevo dole (popř. uprostřed - responzivita):*/}
                    <SnackbarMessage snackInfo={this.state.snackInfo} handleCloseSnackbar={this.handleCloseSnackbar}/>
                    <Loading/>
                </React.Fragment>;


            case LOADING.DONE:
                // Na místě záložek s položkami zvoleného listu se zobrazí buď načítací dialog nebo ty záložky - dle toho, zdali se načítají nebo ne:
                let itemsForShow;
                if (!this.state.loadingItemsFromList) {
                    /*Záložky s vytvořenými položkami:*/
                    itemsForShow = <TabsItem filterValue={this.state.filterValue} items={this.state.items}
                                             filterByValue={this.state.filterBy}
                                             changeDoneStatusItem={this.changeDoneStatusItem}
                                             deleteItem={this.deleteItem} responsiveWidth={this.state.responsiveWidth}
                                             setItemForEdit={this.setItemForEdit}/>
                } else {
                    itemsForShow = <React.Fragment>
                        {/*Na umístění komponenty nezáleží, zpráva se zobrazí vždy vlevo dole (popř. uprostřed - responzivita):*/}
                        <SnackbarMessage snackInfo={this.state.snackInfo}
                                         handleCloseSnackbar={this.handleCloseSnackbar}/>
                        <Loading/>
                    </React.Fragment>;
                }

                return (
                    <React.Fragment>
                        <SnackbarMessage snackInfo={this.state.snackInfo}
                                         handleCloseSnackbar={this.handleCloseSnackbar}/>

                        <div className={"listOverviewDiv"}><ListOverview lists={this.state.lists}
                                                                         selectList={this.selectList}
                                                                         deleteShoppingList={this.deleteShoppingList}
                                                                         isListWithIdSelected={this.isListWithIdSelected}
                                                                         deleteAllShoppingLists={this.deleteAllShoppingLists}
                                                                         createShoppingList={this.createShoppingList}
                                                                         updateShoppingList={this.updateShoppingList}
                                                                         isListNameInUse={this.isListNameInUse}
                                                                         isListNameInUseExceptExisting={this.isListNameInUseExceptExisting}/>
                        </div>


                        <br/>


                        {/*Formulář pro vytvoření nové položky:*/}
                        <FormItem addItem={this.addItem} items={this.state.items} existItem={this.existItem}
                                  deleteAllItems={this.deleteAllItems} responsiveWidth={this.state.responsiveWidth}
                                  ref={this.formItemRef} updateItem={this.updateItem}
                                  checkExistItem={this.checkExistItem}/>

                        <br/>

                        {/*Komponenty pro filtrování položek:*/}
                        <FilterForm setAndFilterValues={this.setAndFilterValues("filterValue")}
                                    setAndFilterByValues={this.setAndFilterValues("filterBy")}
                                    filterByValue={this.state.filterBy}/>

                        <br/>

                        {itemsForShow}
                    </React.Fragment>
                );


            default:
                return <div>
                    <SnackbarMessage snackInfo={this.state.snackInfo} handleCloseSnackbar={this.handleCloseSnackbar}/>
                    <div>Unknown Error.</div>
                </div>;
        }
    };


    render() {
        return <div>{this.getComponentsForRender()}</div>;
    }
}