import React from 'react';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import FormItem from "../FormItem";

// Výchozí hodnota (prázdný text) pro název listu při otevření dialogu:
const defaultListName = "";
// Výchozí "pomocný" text pod textovým polem:
const defaultHelperText = "Unique name of the shopping list";
// Text, který říká, že byl zadán duplicitní název (v proměnné, aby se v komponentě neopakoval na více místech):
const duplicateName = "Shopping list with the specified name already exists";

// Regulární výraz pro validaci názvu listu (stejný jako v cz.uhk.fim.shoppinglist.entity.ShoppingList.name):
const regEx = new RegExp('^[a-záčďéěíňóřšťúůýž]+[áčďéěíňóřšťúůýž\\w\\W\\s\\d,.]{0,299}$', 'i');

/**
 * Dialog, který se zobrazí, když chce uživatel vytvořit nový list nebo upravit existující.
 */

class ListDialog extends React.Component {

    state = {
        // Název list zadaný v textovém poli:
        name: defaultListName,
        // True v případě, že je název listu validní, jinak false:
        isNameValid: true,
        // Text pod textovým polem pro název listu:
        helperText: defaultHelperText,
        // Existující list, který chce uživatel upravit:
        listForEdit: undefined
    };

    // Nastavení listu pro editaci:
    setListForUpdate = async list => {
        await this.setState({listForEdit: list});
        await this.setState({name: list.name});

        // Validace by neměla být potřeba (do DB nelze vložit nevalidní název bez přístupu k DB), ale kdyby náhodou ...
        await this.validateListName(list.name);
    };

    // Uložení změny hodnoty v textovém poli pro název listu:
    handleListNameChange = async event => {
        const tempName = event.target.value;
        await this.setState({name: tempName});

        // Kontrola, zdali je zadaný název validní nebo ne:
        await this.validateListName(tempName);
    };

    // Test, zdali je uživatelem zadaný název listu validní nebo ne, dle toho se nastaví hodnota pro nastavení textového pole do "chybového" módu:
    validateListName = async name => {
        let valid = true;
        let helperTextTemp = defaultHelperText;

        if (name === "") {
            helperTextTemp = "Shopping list name is not specified";
            valid = false;
        } else if (FormItem.containsTextOnlyWhiteSpace(name)) {
            helperTextTemp = "Shopping list name contains only white spaces";
            valid = false;
        } else if (!regEx.test(name)) {
            helperTextTemp = "Shopping list name does not match desired syntax";
            valid = false;
        }

        await this.setState({helperText: helperTextTemp});
        await this.setState({isNameValid: valid});
    };

    handleOk = async () => {
        const {name} = this.state;

        // Při prvním načtení dialogu nemusí být zadáno nic, tak je třeba otestovat i ten prázdný (/výchozí) řetězec:
        await this.validateListName(name);

        // Zjištění, zdali je název v pořádku (nastaví funkce validateListName):
        if (!this.state.isNameValid)
            return;


        // List pro editaci (je li uveden / předán):
        const {listForEdit} = this.state;
        // Pokud list pro editaci není uveden, jedná se o vytvoření nového:
        if (listForEdit === undefined) {
            // Zjištění, zdali již list s názvem v proměnné name existuje nebo ne:
            if (await this.props.isListNameInUse(name)) {
                await this.setState({helperText: duplicateName});
                await this.setState({isNameValid: false});
                return;
            }
            // Vytvoření požadovaného listu:
            await this.props.createShoppingList(name);
        }

        // Zde je list pro editaci přítomen (/ předán), tak se jedná o editaci existujícího listu:
        // (Zjistí se, zdali ještě není název listu využit [nebude se uvažovat název upravovaného listu, případně se upraví])
        else {
            // zjištění, zdali název listu není využit (vynechá se testování názvu toho upravovaného listu:

            // Vytvoření nového objektu pro zaslání v požadavku:
            const tempList = listForEdit;
            tempList.name = name;

            // Zaslání požadavku a nastavení výsledku:
            if (await this.props.isListNameInUseExceptExisting(tempList)) {
                await this.setState({helperText: duplicateName});
                await this.setState({isNameValid: false});
                return;
            }
            // Nastavení prázdného textu (výchozí hodnoty), aby se při otevření dialogu nezobrazovala původní hodnota:
            await this.setState({name: defaultListName});
            // Upravení listu v DB:
            await this.props.updateShoppingList(tempList);
        }
    };

    render() {
        return (
            <Dialog
                open={this.props.open}
                disableBackdropClick
                disableEscapeKeyDown
                maxWidth="xs"
                aria-labelledby="confirmation-dialog-title"
            >

                <DialogTitle id="confirmation-dialog-title">Shopping list</DialogTitle>

                <DialogContent>
                    <TextField
                        id="listName"
                        label="List"
                        value={this.state.name}
                        placeholder="List name"
                        helperText={this.state.helperText}
                        fullWidth={true}
                        autoFocus={true}
                        margin="normal"
                        variant="filled"
                        required={true}
                        error={!this.state.isNameValid}
                        InputLabelProps={{shrink: true}}
                        onChange={this.handleListNameChange}
                    />
                </DialogContent>

                <DialogActions>
                    <Button onClick={this.props.handleOpenListDialog} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleOk} color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default ListDialog;