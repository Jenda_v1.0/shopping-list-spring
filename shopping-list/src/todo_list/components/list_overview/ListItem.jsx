import React from 'react';
import ListItemUi from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import '../styles/ListItem.css';
import Tooltip from '@material-ui/core/Tooltip';

/**
 * Zobrazení jedné položky reprezentující konkrétní nákupní list.
 */

export default class ListItem extends React.Component {

    render() {
        const {list} = this.props;

        if (list === undefined) {
            // "Prázdná hodnota:"
            return <ListItemUi/>;
        }

        return (
            <ListItemUi role={undefined} dense button onClick={this.props.selectList}>
                <Checkbox
                    checked={this.props.checked}
                    disableRipple
                />
                <div className={"listItemTextDiv"}>{list.name}</div>
                <ListItemSecondaryAction>
                    <Tooltip title={"Edit list"}>
                        <IconButton aria-label="Comments" onClick={this.props.openListDialogForEditList}>
                            <EditIcon/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={"Delete list"}>
                        <IconButton aria-label="Delete" onClick={this.props.deleteShoppingList}>
                            <DeleteIcon/>
                        </IconButton>
                    </Tooltip>
                </ListItemSecondaryAction>
            </ListItemUi>
        );
    }
}