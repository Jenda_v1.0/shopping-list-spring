import React from 'react';
import List from '@material-ui/core/List';
import ListItem from './ListItem';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
import '../styles/ListOverview.css'
import ListDialog from "../list_dialog/ListDialog";

/**
 * Pro přehled veškerých vytvořených nákupních listů.
 */

export default class ListOverview extends React.Component {

    state = {
        // Zdali se má otevřít dialog pro vytvoření / editaci listu:
        openListDialog: false
    };

    constructor(props) {
        super(props);

        // Reference na dialog, aby v něm bylo možné zavolat nastavení editace listu:
        this.myRef = React.createRef();
    }

    // Otevření / Uzavření dialogu pro vytvoření / editaci listu:
    handleOpenListDialog = async () => {
        await this.setState({openListDialog: !this.state.openListDialog});
    };

    // Otevření dialogu pro editaci existujícího listu:
    openListDialogForEditList = async list => {
        // Otevření dialogu pro editaci (existujícího) listu (list):
        await this.handleOpenListDialog();
        // Nastavení listu (list) pro editaci v dialogu:
        await this.myRef.current.setListForUpdate(list);
    };

    render() {
        // Vytvoření komponent pro položky:
        const lists = this.props.lists.map(list => {
            // Zdali je aktuálně iterovaný list (ne) označen - dle toho se označí i chcb:
            const checkedList = this.props.isListWithIdSelected(list.id);
            return <ListItem key={list.id} list={list} selectList={() => this.props.selectList(list)}
                             deleteShoppingList={() => this.props.deleteShoppingList(list.id)} checked={checkedList}
                             openListDialogForEditList={() => this.openListDialogForEditList(list)}/>
        });

        return (
            <React.Fragment>
                {/*Dialog pro vytvoření / editaci listu:*/}
                <ListDialog ref={this.myRef} open={this.state.openListDialog}
                            handleOpenListDialog={this.handleOpenListDialog}
                            createShoppingList={this.props.createShoppingList}
                            updateShoppingList={this.props.updateShoppingList}
                            isListNameInUse={this.props.isListNameInUse}
                            isListNameInUseExceptExisting={this.props.isListNameInUseExceptExisting}/>

                <List>
                    {lists}
                </List>
                <div className={"listOverviewButtons"}>
                    <Tooltip title="Add list" aria-label="Add">
                        <Fab size={"small"} color="secondary">
                            <AddIcon onClick={this.handleOpenListDialog}/>
                        </Fab>
                    </Tooltip>
                    <Tooltip title="Delete all lists" aria-label="Delete">
                        <Fab id={"deleteAllListsButton"} size={"small"} aria-label="Delete">
                            <DeleteIcon onClick={this.props.deleteAllShoppingLists}/>
                        </Fab>
                    </Tooltip>
                </div>
            </React.Fragment>
        );
    }
}