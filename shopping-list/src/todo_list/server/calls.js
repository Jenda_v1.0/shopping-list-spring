import axios from 'axios';

let Calls = {

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    async call(method, url, dtoIn) {
        return await axios({
            method: method,
            url: url,
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json"
            },
            data: dtoIn
        });
    },

    getShoppingItemUri: function (useCase) {
        return (
            "http://localhost:8080/shopping-item/" + useCase
            // "/shopping-item/" + useCase// pro spuštění v dockeru
        );
    },

    getShoppingListUri: function (useCase) {
        return (
            "http://localhost:8080/shopping-list/" + useCase
            // "/shopping-list/" + useCase// pro spuětšní v dockeru
        );
    },

    async createShoppingList(list) {
        const commandUri = this.getShoppingListUri("add");
        return await Calls.call("post", commandUri, list);
    },

    async updateShoppingList(list) {
        const commandUri = this.getShoppingListUri("update");
        return await Calls.call("put", commandUri, list);
    },

    async isShoppingListNameInUse(name) {
        const commandUri = this.getShoppingListUri("is-name-used?name=" + name);
        return await Calls.call("post", commandUri, null);
    },

    async isShoppingListNameInUseExcept(list) {
        const commandUri = this.getShoppingListUri("is-list-used");
        return await Calls.call("post", commandUri, list);
    },

    async getShoppingLists() {
        const commandUri = this.getShoppingListUri("all");
        return await Calls.call("get", commandUri, null);
    },

    async getItemsByShoppingListId(listId) {
        const commandUri = this.getShoppingItemUri("by-list-id/" + listId);
        return await Calls.call("get", commandUri, null);
    },

    async deleteShoppingList(listId) {
        const commandUri = this.getShoppingListUri("/" + listId);
        return await Calls.call("delete", commandUri, null);
    },

    async deleteAllShoppingLists() {
        const commandUri = this.getShoppingListUri("all");
        return await Calls.call("delete", commandUri, null);
    },

    async deleteAllItemsByListId(listId) {
        const commandUri = this.getShoppingItemUri("delete-by-list-id/" + listId);
        return await Calls.call("delete", commandUri, null);
    },

    async deleteShoppingItem(id) {
        const commandUri = this.getShoppingItemUri(id);
        return await Calls.call("delete", commandUri, null);
    },

    async createShoppingItem(item) {
        const commandUri = this.getShoppingItemUri("add");
        return await Calls.call("post", commandUri, item);
    },

    async updateShoppingItem(item) {
        const commandUri = this.getShoppingItemUri("update");
        return await Calls.call("put", commandUri, item);
    }
};

export default Calls;