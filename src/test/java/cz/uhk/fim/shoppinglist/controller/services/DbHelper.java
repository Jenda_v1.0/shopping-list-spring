package cz.uhk.fim.shoppinglist.controller.services;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.entity.ShoppingList;

import java.util.List;

/**
 * Výpomocné / společné metody pro testy.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 10.04.2019 13:38
 */

public interface DbHelper {

    void clearDb();

    ShoppingList createShoppingListWithTasks(String name);

    ShoppingList createShoppingListWithDescriptions(String name);

    List<ShoppingList> createCountOfShoppingListWithTask(String name, int count);

    ShoppingItem createShoppingItemTask(ShoppingList list, String name);

    void saveShoppingList(List<ShoppingList> shoppingLists);

    void saveShoppingList(ShoppingList shoppingList);
}