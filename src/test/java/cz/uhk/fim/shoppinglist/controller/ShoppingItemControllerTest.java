package cz.uhk.fim.shoppinglist.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.uhk.fim.shoppinglist.controller.services.DbHelper;
import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.exception.ValidationException;
import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.repo.ShoppingListRepository;
import cz.uhk.fim.shoppinglist.service.ShoppingItemService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestPropertySource("/application_test.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class ShoppingItemControllerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingItemControllerTest.class);

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Autowired
    private ShoppingItemService shoppingItemService;

    @Autowired
    private DbHelper dbHelper;

    private MockMvc mvc;

    @Before
    public void setUp() {
        LOGGER.info("Call setUp method.");

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();


        LOGGER.info("Clear database before test.");
        // Aby byla DB prázdná, jinak by mohly vznikat duplicity (záleží na testu):
        dbHelper.clearDb();
    }

    @After
    public void tearDown() {
        LOGGER.info("Call tearDown method.");
        // Vyprázdnění DB:
        dbHelper.clearDb();
    }


    @Test
    public void add_Test_Ok() {
        LOGGER.info("Test add shopping item using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // přidání nové položky pomocí metody v kontroleru:
        final ShoppingItem fakeItem = dbHelper.createShoppingItemTask(fakeList, "Fake item");

        final ShoppingItem addedItem = shoppingItemService.add(fakeItem);
        assertThat(addedItem, is(fakeItem));
    }

    @Test
    public void add_Test_Not_Ok() throws Exception {
        LOGGER.info("Test add shopping item with invalid id and name using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // přidání nové položky pomocí metody v kontroleru (prázdný název není validní):
        final ShoppingItem fakeItem = dbHelper.createShoppingItemTask(fakeList, "");
        fakeItem.setId(-1L);

        // Převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeItem);

        // Zaslání požadavku na přidání položky:
        mvc.perform(post("/shopping-item/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addNullName_Test_Not_Ok() {
        LOGGER.info("Test add shopping item with invalid (null) name using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // přidání nové položky pomocí metody v kontroleru:
        final ShoppingItem fakeItem = dbHelper.createShoppingItemTask(fakeList, null);

        try {
            shoppingItemService.add(fakeItem);
        } catch (ValidationException e) {
            assertThat("These validation errors have been detected: [Shopping item name must be set]", is(e.getMessage()));
        }
    }

    @Test
    public void addInvalidKindOfItem_Test_Not_Ok() {
        LOGGER.info("Test add shopping item with invalid kindOfItem using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // přidání nové položky pomocí metody v kontroleru:
        final ShoppingItem fakeItem = dbHelper.createShoppingItemTask(fakeList, "Fake Item");
        fakeItem.setKindOfShoppingItem(null);

        try {
            shoppingItemService.add(fakeItem);
        } catch (ValidationException e) {
            assertThat("These validation errors have been detected: [Kind of shopping item must be set]", is(e.getMessage()));
        }
    }

    @Test
    public void addInvalidAmount_Test_Not_Ok() {
        LOGGER.info("Test add shopping item with invalid amount using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // přidání nové položky pomocí metody v kontroleru:
        final ShoppingItem fakeItem = dbHelper.createShoppingItemTask(fakeList, "Fake Item");
        fakeItem.setAmount(-1);

        try {
            shoppingItemService.add(fakeItem);
        } catch (ValidationException e) {
            assertThat("These validation errors have been detected: [Shopping item amount must be greater or equal to zero, found '-1']", is(e.getMessage()));
        }
    }

    @Test
    @Transactional
    public void update_Test_Ok() {
        LOGGER.info("Test update shopping item using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // aktualizace položky pomocí metody v kontroleru:
        final ShoppingItem fakeItem = allLists.get(0).getItems().get(0);
        final String updatedName = "Update item name";
        fakeItem.setName(updatedName);

        final ShoppingItem updatedItem = shoppingItemService.update(fakeItem);
        assertThat(updatedName, is(updatedItem.getName()));
    }

    @Test
    @Transactional
    public void update_Test_Not_Ok() {
        LOGGER.info("Test update shopping item by item with invalid name using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // aktualizace položky pomocí metody v kontroleru (prázdný název není validní):
        final ShoppingItem fakeItem = allLists.get(0).getItems().get(0);
        fakeItem.setName("");

        try {
            shoppingItemService.update(fakeItem);
        } catch (ValidationException e) {
            assertThat("These validation errors have been detected: [Shopping item name must be set, Shopping item name can start with accented or capitalized letter and can only contain accented and capitalized letters, alphanumeric and non-alphanumeric characters, found '']", is(e.getMessage()));
        }
    }

    @Test
    @Transactional
    public void delete_Test_Ok() throws Exception {
        LOGGER.info("Test delete shopping item using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithDescriptions("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        final ShoppingItem shoppingItem = allLists.get(0).getItems().get(0);
        // Zaslání požadavku na smazání položky:
        mvc.perform(delete("/shopping-item/" + shoppingItem.getId()))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);
        assertThat(allItems, hasSize(allLists.get(0).getItems().size() - 1));
    }

    @Test
    @Transactional
    public void delete_Test_Not_Ok() throws Exception {
        LOGGER.info("Test delete invalid shopping item (try delete item with non existing id) using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithDescriptions("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // nalezení id položky, která v DB není, pro test chyby požadavku kontroleru:
        Long notExistingId = allLists.get(0).getItems().get(0).getId();
        while (existShoppingItemWithId(notExistingId)) {
            notExistingId++;
        }

        // Zaslání požadavku na smazání položky:
        mvc.perform(delete("/shopping-item/" + notExistingId))
                // Očekávaný výsledek:
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getItemsByShoppingListId_Test_Ok() throws Exception {
        LOGGER.info("Test get all shopping items by shopping list id using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithDescriptions("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // Zaslání požadavku na získání položky:
        final ResultActions resultActions = mvc.perform(get("/shopping-item/by-list-id/" + fakeList.getId()))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        assertNotNull(contentAsString);

        // položky z response:
        final List<ShoppingItem> shoppingItems = objectMapper.readValue(contentAsString, new TypeReference<List<ShoppingItem>>() {
        });
        assertThat(shoppingItems, hasSize(fakeList.getItems().size()));
    }

    @Test
    public void getItemsByShoppingListId_Test_Not_Ok() throws Exception {
        LOGGER.info("Test get all shopping items by shopping list id with invalid id using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithDescriptions("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // Zaslání požadavku na získání položky:
        final ResultActions resultActions = mvc.perform(get("/shopping-item/by-list-id/" + fakeList.getId() + 1))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        assertNotNull(contentAsString);

        // položky z response:
        final List<ShoppingItem> shoppingItems = objectMapper.readValue(contentAsString, new TypeReference<List<ShoppingItem>>() {
        });
        assertThat(shoppingItems, is(empty()));
    }

    @Test
    public void deleteAllItemsByListId_Test_Ok() throws Exception {
        LOGGER.info("Test delete all shopping items by shopping list id using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithDescriptions("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // Zaslání požadavku na smazání položky:
        mvc.perform(delete("/shopping-item/delete-by-list-id/" + fakeList.getId()))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);
        assertThat(allItems, is(empty()));
    }

    @Test
    public void deleteAllItemsByListId_Test_Not_Ok() throws Exception {
        LOGGER.info("Test delete all shopping items by invalid shopping list id using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithDescriptions("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // Zaslání požadavku na smazání položky:
        mvc.perform(delete("/shopping-item/delete-by-list-id/" + fakeList.getId() + 1))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);
        assertThat(allItems, hasSize(fakeList.getItems().size()));
    }

    private boolean existShoppingItemWithId(Long id) {
        return shoppingItemRepository.findById(id).isPresent();
    }
}