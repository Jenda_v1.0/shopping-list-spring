package cz.uhk.fim.shoppinglist.controller.services;

import cz.uhk.fim.shoppinglist.entity.KindOfShoppingItem;
import cz.uhk.fim.shoppinglist.entity.KindOfUnit;
import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.repo.ShoppingListRepository;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

/**
 * Pomocná třída pro testy.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 10.04.2019 13:39
 */

@Configuration
public class DbHelperImpl implements DbHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbHelperImpl.class);

    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @Override
    public void clearDb() {
        LOGGER.info("Delete all items from database.");

        shoppingItemRepository.deleteAll();
        shoppingListRepository.deleteAll();

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        final List<ShoppingList> allLists = shoppingListRepository.findAll();

        Assert.assertThat(allItems, is(empty()));
        Assert.assertThat(allLists, is(empty()));
    }

    @Override
    public ShoppingList createShoppingListWithTasks(String name) {
        LOGGER.info("Create shopping list with task items.");

        final ShoppingList shoppingList = new ShoppingList();
        shoppingList.setName(name);

        final List<ShoppingItem> shoppingItems = createShoppingListTaskItems(shoppingList);
        shoppingList.setItems(shoppingItems);

        return shoppingList;
    }

    @Override
    public ShoppingList createShoppingListWithDescriptions(String name) {
        LOGGER.info("Create shopping list with descriptions items.");

        final ShoppingList shoppingList = new ShoppingList();
        shoppingList.setName(name);

        final List<ShoppingItem> shoppingItems = createShoppingListDescriptionItems(shoppingList);
        shoppingList.setItems(shoppingItems);

        return shoppingList;
    }

    @Override
    public List<ShoppingList> createCountOfShoppingListWithTask(String name, int count) {
        LOGGER.info("Create {} shopping lists with task items.", count);

        if (count <= 0)
            return new ArrayList<>();

        final List<ShoppingList> lists = new ArrayList<>();
        IntStream.range(0, count).forEach(i -> {
            final ShoppingList shoppingListWithTasks = createShoppingListWithTasks(name + i);
            lists.add(shoppingListWithTasks);
        });

        return lists;
    }


    @Override
    public ShoppingItem createShoppingItemTask(ShoppingList list, String name) {
        final ShoppingItem shoppingItem = new ShoppingItem();
        shoppingItem.setName(name);
        shoppingItem.setNote("This is a task note.");
        shoppingItem.setKindOfShoppingItem(KindOfShoppingItem.TASK);
        shoppingItem.setAmount(5);
        shoppingItem.setKindOfUnit(KindOfUnit.PIECE);
        shoppingItem.setDeadline(createDateInOneHour());
        shoppingItem.setList(list);

        LOGGER.info("Created new shopping item {} to shopping list {}", shoppingItem, list);
        return shoppingItem;
    }

    @Override
    public void saveShoppingList(List<ShoppingList> shoppingLists) {
        shoppingLists.forEach(this::saveShoppingList);
    }

    public void saveShoppingList(ShoppingList shoppingList) {
        LOGGER.info("Save to DB fake shopping list: {}", shoppingList);
        shoppingListRepository.save(shoppingList);
        shoppingList.getItems().forEach(shoppingItem -> shoppingItemRepository.save(shoppingItem));
    }

    private static List<ShoppingItem> createShoppingListTaskItems(ShoppingList list) {
        final List<ShoppingItem> items = new ArrayList<>();

        IntStream.range(0, 5).forEach(i -> {
            final ShoppingItem shoppingItem = new ShoppingItem();
            shoppingItem.setName("List item " + (i + 1));
            shoppingItem.setNote("This is a task note.");
            shoppingItem.setAmount(i);
            shoppingItem.setKindOfUnit(KindOfUnit.PIECE);
            shoppingItem.setKindOfShoppingItem(KindOfShoppingItem.TASK);
            shoppingItem.setDeadline(createDateInOneHour());
            shoppingItem.setList(list);

            items.add(shoppingItem);
            LOGGER.info("Added new shopping item {} to shopping list {}", shoppingItem, list);
        });

        return items;
    }

    private static List<ShoppingItem> createShoppingListDescriptionItems(ShoppingList list) {
        final List<ShoppingItem> items = new ArrayList<>();

        IntStream.range(0, 5).forEach(i -> {
            final ShoppingItem shoppingItem = new ShoppingItem();
            shoppingItem.setName("List item " + (i + 1));
            shoppingItem.setNote("This is a description note.");
            shoppingItem.setKindOfShoppingItem(KindOfShoppingItem.DESCRIPTION);
            shoppingItem.setList(list);

            items.add(shoppingItem);
            LOGGER.info("Added new shopping item {} to shopping list {}", shoppingItem, list);
        });

        return items;
    }

    private static LocalDateTime createDateInOneHour() {
        return LocalDateTime.now().plusHours(1);
    }
}