package cz.uhk.fim.shoppinglist.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.uhk.fim.shoppinglist.controller.services.DbHelper;
import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.repo.ShoppingListRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// Zdroj pro autowired vnořených objektů v kontroléru: https://stackoverflow.com/questions/39892534/testing-a-controller-with-an-auto-wired-component-is-null-when-calling-the-contr

@TestPropertySource("/application_test.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class ShoppingListControllerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingListControllerTest.class);

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Autowired
    private DbHelper dbHelper;

    private MockMvc mvc;


    @Before
    public void setUp() {
        LOGGER.info("Call setUp method.");

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();


        LOGGER.info("Clear database before test.");
        // Aby byla DB prázdná, jinak by mohly vznikat duplicity (záleží na testu):
        dbHelper.clearDb();
    }

    @After
    public void tearDown() {
        LOGGER.info("Call tearDown method.");
        // Vyprázdnění DB:
        dbHelper.clearDb();
    }


    @Test
    public void add_Test_Ok() throws Exception {
        LOGGER.info("Test add shopping list using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");

        // Převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeList);

        mvc.perform(post("/shopping-list/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);
        assertThat(allItems, hasSize(fakeList.getItems().size()));
    }

    @Test
    public void add_Test_Not_Ok() throws Exception {
        LOGGER.info("Test add shopping list with invalid name using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("");

        // Převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeList);

        mvc.perform(post("/shopping-list/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_Test_Ok() throws Exception {
        LOGGER.info("Test update shopping list using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        fakeList.setName("Updated fake list name");

        // Převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeList);
        mvc.perform(put("/shopping-list/update")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingList> allLists2 = shoppingListRepository.findAll();
        assertNotNull(allLists2);
        assertThat(allLists2, hasSize(1));

        final ShoppingList shoppingList = allLists2.get(0);
        assertThat(shoppingList.getName(), is(fakeList.getName()));
    }

    @Test
    public void update_Test_Not_Ok() throws Exception {
        LOGGER.info("Test update shopping list with invalid name using controller method.");

        final String testName = "Fake List";
        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks(testName);
        dbHelper.saveShoppingList(fakeList);

        // kontrola uložení listu:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        // prázdný název není validní:
        fakeList.setName("");

        // převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeList);
        mvc.perform(put("/shopping-list/update")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isBadRequest());

        // kontrola, že se neuložily změny:
        final List<ShoppingList> allLists2 = shoppingListRepository.findAll();
        assertNotNull(allLists2);
        assertThat(allLists2, hasSize(1));

        final ShoppingList shoppingList = allLists2.get(0);
        assertThat(shoppingList.getName(), is(testName));
    }

    @Test
    public void getAll_Test_Ok() throws Exception {
        LOGGER.info("Test get all shopping lists using controller method.");

        final int countOfLists = 5;
        final List<ShoppingList> fakeLists = dbHelper.createCountOfShoppingListWithTask("Fake List ", countOfLists);
        assertNotNull(fakeLists);
        assertThat(fakeLists, hasSize(countOfLists));

        // uložení do DB:
        dbHelper.saveShoppingList(fakeLists);

        // kontrola uložených hodnot - není nezbytné, ...
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(fakeLists.size()));

        final ResultActions resultActions = mvc.perform(get("/shopping-list/all"))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        final List<ShoppingList> allLists2 = objectMapper.readValue(contentAsString, new TypeReference<List<ShoppingList>>() {
        });

        assertNotNull(allLists2);
        assertThat(allLists2, hasSize(fakeLists.size()));
    }

    @Test
    public void delete_Test_Ok() throws Exception {
        LOGGER.info("Test delete shopping list using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola a načtení id - čka vytvořeného listu pro předání jako parametru pro smazání:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        fakeList.setId(allLists.get(0).getId());

        // Převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeList);
        mvc.perform(delete("/shopping-list/" + fakeList.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingList> allLists2 = shoppingListRepository.findAll();
        assertNotNull(allLists2);
        assertThat(allLists2, is(empty()));

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);
        assertThat(allItems, is(empty()));
    }

    @Test
    public void delete_Test_Not_Ok() throws Exception {
        LOGGER.info("Test delete shopping list with invalid list id using controller method.");

        final ShoppingList fakeList = dbHelper.createShoppingListWithTasks("Fake List");
        dbHelper.saveShoppingList(fakeList);

        // kontrola a načtení id - čka vytvořeného listu pro předání jako parametru pro smazání:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(1));

        fakeList.setId(allLists.get(0).getId());

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);
        assertThat(allItems, hasSize(fakeList.getItems().size()));

        // Převedení objektu do JSONu:
        final String body = objectMapper.writeValueAsString(fakeList);
        // mělo by vše skončit v pořádku, id položky neeixstuje, takže z DB se nic nesmaže:
        mvc.perform(delete("/shopping-list/" + fakeList.getId() + 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        // test, že v DB je pořád vše uvedeno:
        final List<ShoppingList> allLists2 = shoppingListRepository.findAll();
        assertNotNull(allLists2);
        assertThat(allLists2, hasSize(1));

        final List<ShoppingItem> allItems2 = shoppingItemRepository.findAll();
        assertNotNull(allItems2);
        assertThat(allItems2, hasSize(fakeList.getItems().size()));
    }

    @Test
    public void deleteAll_Test_Ok() throws Exception {
        LOGGER.info("Test delete all shopping list using controller method.");

        final int countOfLists = 15;
        final List<ShoppingList> fakeLists = dbHelper.createCountOfShoppingListWithTask("Fake List ", countOfLists);
        assertNotNull(fakeLists);
        assertThat(fakeLists, hasSize(countOfLists));

        // uložení do DB:
        dbHelper.saveShoppingList(fakeLists);

        // kontrola uložených hodnot:
        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(countOfLists));

        final List<ShoppingItem> allItems = shoppingItemRepository.findAll();
        assertNotNull(allItems);

        // zjištění počtu položek v nákupních listech:
        int countOfItems = 0;
        for (ShoppingList list : fakeLists) {
            countOfItems += list.getItems().size();
        }
        assertThat(allItems, hasSize(countOfItems));

        mvc.perform(delete("/shopping-list/all"))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final List<ShoppingList> allLists2 = shoppingListRepository.findAll();
        final List<ShoppingItem> allItems2 = shoppingItemRepository.findAll();

        assertNotNull(allLists2);
        assertNotNull(allItems2);

        assertThat(allLists2, is(empty()));
        assertThat(allItems2, is(empty()));
    }

    @Test
    // pro jméno listu je využito:
    public void isListNameUsed_Test_Ok() throws Exception {
        LOGGER.info("Test check if shopping list name is already in use using controller method.");

        final int countOfLists = 10;
        final List<ShoppingList> fakeLists = dbHelper.createCountOfShoppingListWithTask("Fake List ", countOfLists);
        assertNotNull(fakeLists);
        assertThat(fakeLists, hasSize(countOfLists));

        dbHelper.saveShoppingList(fakeLists);

        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(countOfLists));

        final ResultActions resultActions = mvc.perform(post("/shopping-list/is-name-used")
                .param("name", fakeLists.get(0).getName()))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        assertNotNull(contentAsString);

        final boolean result = Boolean.parseBoolean(contentAsString);
        assertTrue(result);
    }

    @Test
    // pro jméno listu ještě není využito:
    public void isListNameUsed2_Test_Ok() throws Exception {
        LOGGER.info("Test check if shopping list name is not in use using controller method.");

        final int countOfLists = 10;
        final List<ShoppingList> fakeLists = dbHelper.createCountOfShoppingListWithTask("Fake List ", countOfLists);
        assertNotNull(fakeLists);
        assertThat(fakeLists, hasSize(countOfLists));

        dbHelper.saveShoppingList(fakeLists);

        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(countOfLists));

        final ResultActions resultActions = mvc.perform(post("/shopping-list/is-name-used")
                .param("name", "some name"))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        assertNotNull(contentAsString);

        final boolean result = Boolean.parseBoolean(contentAsString);
        assertFalse(result);
    }

    @Test
    // zdali je název listu využit ještě v jiném listu než s listem s určitým id - tento list nebude zahrnut do testu
    public void isListNameUsedExcept_Test_Ok() throws Exception {
        LOGGER.info("Test check if shopping list name is in use (except specific list) using controller method.");

        final int countOfLists = 10;
        final List<ShoppingList> fakeLists = dbHelper.createCountOfShoppingListWithTask("Fake List ", countOfLists);
        assertNotNull(fakeLists);
        assertThat(fakeLists, hasSize(countOfLists));

        dbHelper.saveShoppingList(fakeLists);

        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(countOfLists));

        final String body = objectMapper.writeValueAsString(fakeLists.get(0));
        final ResultActions resultActions = mvc.perform(post("/shopping-list/is-list-used")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        assertNotNull(contentAsString);

        final boolean result = Boolean.parseBoolean(contentAsString);
        assertFalse(result);
    }

    @Test
    // zdali je název listu využit ještě v jiném listu než s listem s určitým id - tento list nebude zahrnut do testu
    // test nastaví jméno listu, který je již využit
    public void isListNameUsedExcept2_Test_Ok() throws Exception {
        LOGGER.info("Test check if shopping list name is in use using controller method.");

        final int countOfLists = 10;
        final List<ShoppingList> fakeLists = dbHelper.createCountOfShoppingListWithTask("Fake List ", countOfLists);
        assertNotNull(fakeLists);
        assertThat(fakeLists, hasSize(countOfLists));

        dbHelper.saveShoppingList(fakeLists);

        final List<ShoppingList> allLists = shoppingListRepository.findAll();
        assertNotNull(allLists);
        assertThat(allLists, hasSize(countOfLists));

        final ShoppingList shoppingList = fakeLists.get(0);
        shoppingList.setName("Fake List 2");// s indexem 2 by měl být využit (využito 0 - 10):
        final String body = objectMapper.writeValueAsString(shoppingList);
        final ResultActions resultActions = mvc.perform(post("/shopping-list/is-list-used")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(body))
                // Očekávaný výsledek:
                .andExpect(status().isOk());

        final String contentAsString = resultActions.andReturn().getResponse().getContentAsString();
        assertNotNull(contentAsString);

        final boolean result = Boolean.parseBoolean(contentAsString);
        assertTrue(result);
    }
}