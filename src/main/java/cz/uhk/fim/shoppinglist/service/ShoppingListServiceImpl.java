package cz.uhk.fim.shoppinglist.service;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.exception.ShoppingListNotFound;
import cz.uhk.fim.shoppinglist.exception.ValidationException;
import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.repo.ShoppingListRepository;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import cz.uhk.fim.shoppinglist.validation.validator.shopping_list.ShoppingListValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Implementace CRUD operací (nad DB) s nákupním listem / seznamem {@link cz.uhk.fim.shoppinglist.entity.ShoppingList}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.03.2019 0:22
 */

@Component
public class ShoppingListServiceImpl implements ShoppingListService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingListServiceImpl.class);

    private final ShoppingListRepository shoppingListRepository;
    private final ShoppingItemRepository shoppingItemRepository;

    private final ShoppingListValidator shoppingListValidator;

    @Autowired
    public ShoppingListServiceImpl(ShoppingListRepository shoppingListRepository, ShoppingItemRepository shoppingItemRepository, ShoppingListValidator shoppingListValidator) {
        this.shoppingListRepository = shoppingListRepository;
        this.shoppingItemRepository = shoppingItemRepository;
        this.shoppingListValidator = shoppingListValidator;
    }


    @Override
    public ShoppingList add(ShoppingList shoppingList) {
        LOGGER.info("Add shopping list. List: {}", shoppingList);
        validate(shoppingList);

        final ShoppingList savedShoppingList = shoppingListRepository.save(shoppingList);
        // uložení všech položek v listu:
        if (shoppingList.getItems() != null) {
            shoppingList.getItems().forEach(shoppingItemRepository::save);
        }
        return savedShoppingList;
    }

    @Override
    public ShoppingList update(ShoppingList shoppingList) {
        LOGGER.info("Update shopping item. Item: {}", shoppingList);
        validate(shoppingList);

        // Je třeba načíst aktuální položky (k listu shoppingList) z DB a nastavit je do proměnné shoppingList, protože se například při editaci nákupní položky nenastaví hodnoty (listy) uložené na frontendu, proto je zde potřeba pracovat vždy s aktuálními hodnotami:
        final Optional<ShoppingList> byId = shoppingListRepository.findById(shoppingList.getId());
        final ShoppingList shoppingListById = byId.orElseThrow(() -> {
            LOGGER.info("Shopping list {} was not found.", shoppingList);
            return new ShoppingListNotFound("Shopping list with id: '" + shoppingList.getId() + " was not found'");
        });

        shoppingList.setItems(shoppingListById.getItems());
        return shoppingListRepository.save(shoppingList);
    }

    @Override
    public Iterable<ShoppingList> getAll() {
        LOGGER.info("Get all shopping lists from DB.");
        return shoppingListRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("Delete shopping list (and it´s items) by it´s id ('{}')", id);

        shoppingItemRepository.deleteItemsByListId(id);

        if (shoppingListRepository.findById(id).isPresent())// aby nevypadla výjimka, že položka neexistuje - u smazání nemá smysl vracet, že již neexistuje
            shoppingListRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        LOGGER.info("Delete all shopping lists (and all it´s items)");
        shoppingItemRepository.deleteAll();
        shoppingListRepository.deleteAll();
    }

    @Override
    public boolean isListNameUsed(String name) {
        LOGGER.info("Check whether the list name is already in use or not. List name: {}", name);

        final int countOfListsWithName = shoppingListRepository.getCountOfListsWithName(name);
        LOGGER.info("Found {} shopping lists named: {}", countOfListsWithName, name);

        return countOfListsWithName > 0;
    }

    @Override
    public boolean isListNameUsed(ShoppingList shoppingList) {
        LOGGER.info("Check whether the list name is already in use or not. Test will not include a list in the parameter. List: {}", shoppingList);

        validate(shoppingList);

        final int countOfListsWithName = shoppingListRepository.getCountOfListsWithName(shoppingList.getName(), shoppingList.getId());
        LOGGER.info("Found {} shopping lists named: {} without list with id: {}", countOfListsWithName, shoppingList.getName(), shoppingList.getId());

        return countOfListsWithName > 0;
    }

    /**
     * Zvalidování nákupního listu.
     *
     * @param shoppingList
     *         nákupní list, jehož položky / atributy se mají zvalidovat.
     */
    private void validate(ShoppingList shoppingList) {
        final ValidationResult result = shoppingListValidator.validate(shoppingList);

        if (!result.isValid()) {
            String message = String.format("These validation errors have been detected: %s", result.getErrors());
            LOGGER.error(message);
            throw new ValidationException(message);
        }
    }
}