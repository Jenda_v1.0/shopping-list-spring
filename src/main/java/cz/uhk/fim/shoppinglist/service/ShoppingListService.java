package cz.uhk.fim.shoppinglist.service;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;

/**
 * Provádění operací (CRUD) nad nákupním listem / seznamem ({@link cz.uhk.fim.shoppinglist.entity.ShoppingList}).
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.03.2019 0:19
 */

public interface ShoppingListService {

    ShoppingList add(ShoppingList shoppingList);

    ShoppingList update(ShoppingList shoppingList);

    Iterable<ShoppingList> getAll();

    void delete(Long id);

    void deleteAll();

    /**
     * Zjištění, zdali je název listu již využit nebo ne.
     *
     * @param name
     *         název listu, o kterém se má zjistit, zdali je již využit nebo ne.
     *
     * @return true v případě, že je název listu již využit, jinak false.
     */
    boolean isListNameUsed(String name);


    /**
     * Zjištění, zdali je název listu v shoppingList již využit nebo ne.
     *
     * <i>Název listu se bude zjišťovat ze všech existujících listů v výjimkou shoppingLIst v parametru.</i>
     *
     * <i>jedná se o zjištění při editaci existujícího listu a v pokud by se uživatel rozhodl ponechat název (nemělo /
     * nemuselo by), nešlo by to uložit, protože by příslušný list již existoval.</i>
     *
     * @param shoppingList
     *         nákupní list, jehož název se má porovnat se všemi ostatními s výjimkou tohoto (testování dle id).
     *
     * @return true v případě, že název nákupního listu shoppingList dostud není využit, jinak false.
     */
    boolean isListNameUsed(ShoppingList shoppingList);
}