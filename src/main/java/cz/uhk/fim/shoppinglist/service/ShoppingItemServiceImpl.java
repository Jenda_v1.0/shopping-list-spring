package cz.uhk.fim.shoppinglist.service;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.exception.ValidationException;
import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import cz.uhk.fim.shoppinglist.validation.validator.shopping_item.ShoppingItemValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

/**
 * Implementace CRUD operací (nad DB) s nákupní položkou {@link ShoppingItem}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 20:04
 */

@Component
public class ShoppingItemServiceImpl implements ShoppingItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingItemServiceImpl.class);

    private final ShoppingItemRepository shoppingItemRepository;

    private final ShoppingItemValidator shoppingItemValidator;

    @Autowired
    public ShoppingItemServiceImpl(ShoppingItemRepository shoppingItemRepository, ShoppingItemValidator shoppingItemValidator) {
        this.shoppingItemRepository = shoppingItemRepository;
        this.shoppingItemValidator = shoppingItemValidator;
    }


    @Override
    public ShoppingItem add(ShoppingItem shoppingItem) {
        LOGGER.info("Add shopping item. Item: {}", shoppingItem);
        validate(shoppingItem);
        return shoppingItemRepository.save(shoppingItem);
    }

    @Override
    public ShoppingItem update(ShoppingItem shoppingItem) {
        LOGGER.info("Update shopping item. New item: {}", shoppingItem);
        validate(shoppingItem);
        return shoppingItemRepository.save(shoppingItem);
    }

    @Override
    public void delete(Long id) {
        LOGGER.info("Delete shopping item by it´s id ('{}')", id);
        shoppingItemRepository.deleteById(id);
    }

    @Override
    public void deleteItemsByListId(Long listId) {
        LOGGER.info("Delete all shopping items by list id {}.", listId);
        shoppingItemRepository.deleteItemsByListId(listId);
    }

    @Override
    public Iterable<ShoppingItem> getAll() {
        LOGGER.info("Get all shopping items from DB.");
        // Položky budou seřazené dle názvu (name), (sestupně dle abecedy):
        return shoppingItemRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }

    @Override
    public Iterable<ShoppingItem> getItemsByShoppingListId(Long listId) {
        LOGGER.info("Get all shopping items by list id: {} from DB.", listId);
        return shoppingItemRepository.findByListId(listId);
    }

    /**
     * Zvalidování položky nákupního listu.
     *
     * @param shoppingItem
     *         položka nákupního listu, jejiž položky / atributy se mají zvalidovat.
     */
    private void validate(ShoppingItem shoppingItem) {
        final ValidationResult result = shoppingItemValidator.validate(shoppingItem);

        if (!result.isValid()) {
            String message = String.format("These validation errors have been detected: %s", result.getErrors());
            LOGGER.error(message);
            throw new ValidationException(message);
        }
    }
}