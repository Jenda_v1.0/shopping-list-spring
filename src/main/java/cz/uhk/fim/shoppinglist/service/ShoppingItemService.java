package cz.uhk.fim.shoppinglist.service;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;

/**
 * Provádění operací (CRUD) nad nákupní položkou ({@link ShoppingItem}).
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 20:04
 */

public interface ShoppingItemService {

    /**
     * Vytvoření položky nákupního košíku.
     *
     * @param shoppingItem
     *         nová nákupní položka pro vložení do DB.
     *
     * @return položku shoppingItem načtenou z DB (s vygenerovaným id apod.).
     */
    ShoppingItem add(ShoppingItem shoppingItem);

    /**
     * Aktualizace hodnot nákupní položky.
     *
     * @param shoppingItem
     *         nákupní položka s novými hodnotami.
     *
     * @return upravenou nákupní položku shoppingItem.
     */
    ShoppingItem update(ShoppingItem shoppingItem);

    /**
     * Vymazání nákupní položky s id z DB.
     *
     * @param id
     *         položky, která se má vymazat z DB.
     */
    void delete(Long id);

    /**
     * Vymazání veškerých nákupních položek, které patří k listu s id listId.
     *
     * @param listId
     *         id listu, jehož veškeré nákupní položky se mají vymazat.
     */
    void deleteItemsByListId(Long listId);

    /**
     * Načtení veškerých nákupních položek z DB.
     *
     * @return veškeré nákupní položky, které se v DB nachází.
     */
    Iterable<ShoppingItem> getAll();

    /**
     * Načtení veškerých nákupních položek, které patří do nákupního listu s listId.
     *
     * @param listId
     *         id nákupního listu, jehož položky se mají načíst.
     *
     * @return položky nákupního listu s listId.
     */
    Iterable<ShoppingItem> getItemsByShoppingListId(Long listId);
}