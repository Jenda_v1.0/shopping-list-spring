package cz.uhk.fim.shoppinglist.validation.rule.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationRule;

/**
 * Rozhraní musí implementovat veškeré třídy sloužící jako pravidla pro validací konkrétního atributu objektu {@link
 * cz.uhk.fim.shoppinglist.entity.ShoppingItem}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:10
 */

public interface ShoppingItemValidationRule extends ValidationRule<ShoppingItem> {
}