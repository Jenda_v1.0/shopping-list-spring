package cz.uhk.fim.shoppinglist.validation.rule.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace nastaveného id konkrétní položky.
 *
 * <i>Může se jednat pouze o přirozené číslo (začínající od 1).</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 17:59
 */

@Component
public class IdMustBePositiveValueRule implements ShoppingItemValidationRule {

    @Override
    public ValidationResult validate(ShoppingItem shoppingItem) {
        final ValidationResult result = new ValidationResult();

        final Long id = shoppingItem.getId();
        if (id != null && id < 1) {
            result.addError(String.format("Shopping item id must be greater then zero, found '%s'", id));
        }

        return result;
    }
}