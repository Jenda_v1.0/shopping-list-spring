package cz.uhk.fim.shoppinglist.validation.rule.shopping_list;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.validation.ValidationRule;

/**
 * Rozhraní musí implementovat veškeré třídy sloužící jako pravidla pro validací konkrétního atributu objektu {@link
 * cz.uhk.fim.shoppinglist.entity.ShoppingList}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:11
 */

public interface ShoppingListValidationRule extends ValidationRule<ShoppingList> {
}