package cz.uhk.fim.shoppinglist.validation;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Výsledek validace.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:05
 */

public class ValidationResult {

    @Getter
    private List<String> errors;

    public ValidationResult() {
        errors = new ArrayList<>();
    }

    public void addError(String error) {
        Objects.requireNonNull(error);
        errors.add(error);
    }

    public void addResult(ValidationResult validationResult) {
        Objects.requireNonNull(validationResult);
        errors.addAll(validationResult.getErrors());
    }

    public boolean isValid() {
        return errors.isEmpty();
    }
}