package cz.uhk.fim.shoppinglist.validation.validator.shopping_list;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;

/**
 * Validace konkrétního objektu {@link ShoppingList}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:01
 */

public interface ShoppingListValidator {

    /**
     * Validace objektu shoppingList.
     *
     * @param shoppingList
     *         objekt, jehož položky se mají zvalidavat, tzn., aplikují se na něj veškerá pravidla pro příslušný
     *         objekt.
     *
     * @return objektu obsahující výsledek validace.
     */
    ValidationResult validate(ShoppingList shoppingList);
}