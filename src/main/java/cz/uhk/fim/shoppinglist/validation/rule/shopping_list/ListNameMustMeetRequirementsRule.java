package cz.uhk.fim.shoppinglist.validation.rule.shopping_list;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace názvu nákupního listu.
 *
 * <i>Název musí být vždy uveden, nesmí se jedná o prázdný text, musí obsahovat maximálně 300 znaků a musí splňovat
 * regulární výrazy.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:30
 */

@Component
public class ListNameMustMeetRequirementsRule implements ShoppingListValidationRule {

    @Override
    public ValidationResult validate(ShoppingList shoppingList) {
        final ValidationResult result = new ValidationResult();

        final String name = shoppingList.getName();

        if (name.isEmpty()) {
            result.addError("Shopping list name must not be empty");
        }

        if (name.length() >= 300) {
            result.addError(String.format("Shopping list name length can be only in between 1 and 300 inclusive, actual length is %s", name.length()));
        }

        if (!name.matches("^(?i)[a-záčďéěíňóřšťúůýž]+[áčďéěíňóřšťúůýž\\w\\W\\s\\d,.]{0,299}$")) {
            result.addError(String.format("Shopping list name can start with accented or capitalized letter and can only contain accented and capitalized letters, alphanumeric and non-alphanumeric characters, found: '%s'", name));
        }

        return result;
    }
}