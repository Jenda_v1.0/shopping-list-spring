package cz.uhk.fim.shoppinglist.validation.rule.shopping_list;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace nastaveného id konkrétní položky.
 *
 * <i>Může se jednat pouze o přirozené číslo (začínající od 1).</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:29
 */

@Component
public class IdIsPositiveValueRule implements ShoppingListValidationRule {

    @Override
    public ValidationResult validate(ShoppingList shoppingList) {
        final ValidationResult result = new ValidationResult();

        final Long id = shoppingList.getId();
        if (id != null && id < 1) {
            result.addError(String.format("Shopping list is id must be greater or equal to 1, found '%s'", id));
        }

        return result;
    }
}