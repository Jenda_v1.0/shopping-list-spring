package cz.uhk.fim.shoppinglist.validation.validator.shopping_list;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import cz.uhk.fim.shoppinglist.validation.rule.shopping_list.ShoppingListValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Provedení validace objektu {@link ShoppingList}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:04
 */

@Component
public class ShoppingListValidatorImpl implements ShoppingListValidator {

    private final List<ShoppingListValidationRule> validationRules;

    public ShoppingListValidatorImpl(List<ShoppingListValidationRule> validationRules) {
        this.validationRules = validationRules;
    }

    @Override
    public ValidationResult validate(ShoppingList shoppingList) {
        final ValidationResult result = new ValidationResult();

        validationRules.forEach(shoppingListValidationRule -> result.addResult(shoppingListValidationRule.validate(shoppingList)));

        return result;
    }
}