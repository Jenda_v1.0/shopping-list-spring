package cz.uhk.fim.shoppinglist.validation;

/**
 * Metodu musí implementovat veškeré třídy sloužíci jako "pravidla" pro validaci nebo rozhraní, které slouží pro
 * oddělení konkrétních pravidel (pravidla pro validací {@link cz.uhk.fim.shoppinglist.entity.ShoppingItem} nebo {@link
 * cz.uhk.fim.shoppinglist.entity.ShoppingList}).
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:13
 */

public interface ValidationRule<T> {

    /**
     * Provedení validace hodnoty dle konkrétního pravidla implementující toto rozhraní.
     *
     * @param t
     *         {@link cz.uhk.fim.shoppinglist.entity.ShoppingList} nebo {@link cz.uhk.fim.shoppinglist.entity.ShoppingItem}
     *         jehož specifická hodnota / atribut má být zvalidován dle konkrétního pravidla implementující toto
     *         rozhraní.
     *
     * @return objekt obsahující výsledek validace.
     */
    ValidationResult validate(T t);
}