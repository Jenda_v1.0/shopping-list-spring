package cz.uhk.fim.shoppinglist.validation.rule.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace nastaveného typu položky, tzn., zdali se jedná o položku typu úkol nebo popisek.
 *
 * <i>Tato informace musí být vždy nastavena.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 18:02
 */

@Component
public class KindOfShoppingItemMustNotBeNullRule implements ShoppingItemValidationRule {

    @Override
    public ValidationResult validate(ShoppingItem shoppingItem) {
        final ValidationResult result = new ValidationResult();

        if (shoppingItem.getKindOfShoppingItem() == null) {
            result.addError("Kind of shopping item must be set");
        }

        return result;
    }
}