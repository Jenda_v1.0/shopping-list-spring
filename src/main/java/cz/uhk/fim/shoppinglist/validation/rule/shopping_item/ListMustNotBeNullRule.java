package cz.uhk.fim.shoppinglist.validation.rule.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace nataveného listu v položce. Tzn., že každá položka musí mít vždy uveden list, do kterého patří.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 18:03
 */

@Component
public class ListMustNotBeNullRule implements ShoppingItemValidationRule {

    @Override
    public ValidationResult validate(ShoppingItem shoppingItem) {
        final ValidationResult result = new ValidationResult();

        if (shoppingItem.getList() == null) {
            result.addError("Shopping item list must be set");
        }

        return result;
    }
}