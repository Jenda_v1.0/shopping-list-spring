package cz.uhk.fim.shoppinglist.validation.rule.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace množství kusů položek.
 *
 * <i>Množství může být pouze přirozené číslo.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 18:01
 */

@Component
public class AmountMustBeZeroOrGreaterRule implements ShoppingItemValidationRule {

    @Override
    public ValidationResult validate(ShoppingItem shoppingItem) {
        final ValidationResult result = new ValidationResult();

        final Integer amount = shoppingItem.getAmount();
        if (amount != null && amount < 0) {
            result.addError(String.format("Shopping item amount must be greater or equal to zero, found '%s'", amount));
        }

        return result;
    }
}