package cz.uhk.fim.shoppinglist.validation.validator.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import cz.uhk.fim.shoppinglist.validation.rule.shopping_item.ShoppingItemValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Provedení validace objektu {@link ShoppingItem}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:21
 */

@Component
public class ShoppingItemValidatorImpl implements ShoppingItemValidator {

    private final List<ShoppingItemValidationRule> validationRules;

    public ShoppingItemValidatorImpl(List<ShoppingItemValidationRule> validationRules) {
        this.validationRules = validationRules;
    }

    @Override
    public ValidationResult validate(ShoppingItem shoppingItem) {
        final ValidationResult result = new ValidationResult();

        validationRules.forEach(shoppingItemValidationRule -> result.addResult(shoppingItemValidationRule.validate(shoppingItem)));

        return result;
    }
}