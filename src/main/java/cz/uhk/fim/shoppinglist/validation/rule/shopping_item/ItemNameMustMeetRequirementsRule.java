package cz.uhk.fim.shoppinglist.validation.rule.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;
import org.springframework.stereotype.Component;

/**
 * Validace názvu položky nákupního listu.
 *
 * <i>Název musí být vždy uveden, nesmí se jednat o prázdný text, musí obsahovat maximálně 300 znaků a musí splňovat
 * regulární výrazy.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 18:00
 */

@Component
public class ItemNameMustMeetRequirementsRule implements ShoppingItemValidationRule {

    @Override
    public ValidationResult validate(ShoppingItem shoppingItem) {
        final ValidationResult result = new ValidationResult();

        final String name = shoppingItem.getName();
        if (name == null || name.isEmpty()) {
            result.addError("Shopping item name must be set");
        }

        if (name != null && name.length() >= 300) {
            result.addError(String.format("Shopping item name length can be only in between 1 and 300 inclusive, actual length is '%s'", name.length()));
        }

        if (name != null && !name.matches("^(?i)[a-záčďéěíňóřšťúůýž]+[áčďéěíňóřšťúůýž\\w\\W\\s\\d,.]{0,299}$")) {
            result.addError(String.format("Shopping item name can start with accented or capitalized letter and can only contain accented and capitalized letters, alphanumeric and non-alphanumeric characters, found '%s'", name));
        }

        return result;
    }
}