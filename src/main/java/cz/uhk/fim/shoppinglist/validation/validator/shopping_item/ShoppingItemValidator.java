package cz.uhk.fim.shoppinglist.validation.validator.shopping_item;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.validation.ValidationResult;

/**
 * Validace konkrétního objektu {@link ShoppingItem}.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:02
 */

public interface ShoppingItemValidator {

    /**
     * Validace objektu shoppingItem.
     *
     * @param shoppingItem
     *         objekt, jehož položky se mají zvalidavat, tzn. aplikují se na něj veškerá pravidla pro příslušný objekt.
     *
     * @return objektu obsaující výsledek validace.
     */
    ValidationResult validate(ShoppingItem shoppingItem);
}
