package cz.uhk.fim.shoppinglist.entity;

/**
 * Priority nákupní položky (například ve smyslu, jak je ta položka důležitá - ji koupit apod.).
 *
 * <i>Při přejmenování výčtové hodnoty je třeba přepsat i položku v src/main/webapp/WEB-INF/view/shopping-list/src/todo_list/constants/Priorities.js
 * (v db_value) - jedná se o hodnotu, dle které se rozeznávají hodnoty v DB.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 18:52
 */

public enum Priority {

    NONE, MUST_BE_FULFILLED, NECESSARILY, GOOD_TO_MEET, HURRIES, DOES_NOT_HURRY, WHEN_IT_IS_FULFILLED_IT_WILL_BE_FULFILLED
}