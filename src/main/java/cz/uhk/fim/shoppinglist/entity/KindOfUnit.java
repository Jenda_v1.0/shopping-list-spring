package cz.uhk.fim.shoppinglist.entity;

/**
 * Typ jednotek / množství příslušné nákupní položky.
 *
 * <i>Při přejmenování výčtové hodnoty je třeba přejmenovat i hodnotu v src/main/webapp/WEB-INF/view/shopping-list/src/todo_list/constants/Units.js
 * (v db_value) - jedná se o název, dle kterého se rozeznávají položky v DB.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 18:51
 */

public enum KindOfUnit {

    PIECE, KILOGRAM, DECAGRAM, GRAMME, POUND
}