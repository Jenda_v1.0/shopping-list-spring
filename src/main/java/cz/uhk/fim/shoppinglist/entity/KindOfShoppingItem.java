package cz.uhk.fim.shoppinglist.entity;

/**
 * Značí typ nákupní položky.
 *
 * <i>Není nezbytné, spíše doplněk. Může se jednat například o nějaký úkol, nebo poznámku, tedy úkol, který se má
 * splnit a chceme vědět, zdali je splněn nebo ne (s komponentou {@link javax.swing.JCheckBox} - ale v React aplikaci),
 * nebo poznámku, která bude spíše pro zobrazení informace pro splnění.</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 05.03.2019 22:13
 */

public enum KindOfShoppingItem {

    /**
     * Bude zobrazen chcb pro nastavení splnění úkolu.
     */
    TASK,

    /**
     * Nebude zobrazen chcb, pouze "poznámka".
     */
    DESCRIPTION
}