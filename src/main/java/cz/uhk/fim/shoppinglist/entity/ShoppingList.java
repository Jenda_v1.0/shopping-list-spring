package cz.uhk.fim.shoppinglist.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Entita reprezentující nákupní list.
 *
 * <i>List může a nemusí obsahovat N nákupních položek ({@link ShoppingItem}).</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 12.03.2019 23:46
 */

@Data
@NoArgsConstructor
@ToString(exclude = "items")
@Entity
@Table(name = "lists")
public class ShoppingList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Positive(message = "Id must be positive number")
//    @Min(value = 1, message = "Id must be greater than or equal to 1")
//    @Max(value = Long.MAX_VALUE, message = "Id must be less or equal to " + Long.MAX_VALUE)
    private Long id;

    @Column(nullable = false)
//    @NotNull(message = "Name must not be blank")
//    @Pattern(regexp = "^(?i)[a-záčďéěíňóřšťúůýž]+[áčďéěíňóřšťúůýž\\w\\W\\s\\d,.]{0,299}$", message = "Name can start with accented or capitalized letter and can only contain accented and capitalized letters, alphanumeric and non-alphanumeric characters")
//    @Size(min = 1, max = 300, message = "Characters length in name can be only in between 1 and 300 inclusive")
    private String name;

    @OneToMany(mappedBy = "list")
    @JsonManagedReference// Pouze u této anotace se při serializaci ponechají data, ne u JsonBackReference
    //alternativa pro "JsonManagedReference": @JsonIgnore
    private List<ShoppingItem> items;
}