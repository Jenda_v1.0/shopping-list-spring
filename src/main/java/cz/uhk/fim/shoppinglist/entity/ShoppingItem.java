package cz.uhk.fim.shoppinglist.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Entita reprezentující položku nákupního listu.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 18:44
 */

@Data
@NoArgsConstructor
@ToString(exclude = "list")
@Entity
@Table(name = "items")
public class ShoppingItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Positive(message = "Id must be positive number")
//    @Min(value = 1, message = "Id must be greater than or equal to 1")
//    @Max(value = Long.MAX_VALUE, message = "Id must be less or equal to " + Long.MAX_VALUE)
    private Long id;

    @Column(nullable = false)
//    @NotNull(message = "Name must not be blank")
//    @Pattern(regexp = "^(?i)[a-záčďéěíňóřšťúůýž]+[áčďéěíňóřšťúůýž\\w\\W\\s\\d,.]{0,299}$", message = "Name can start with accented or capitalized letter and can only contain accented and capitalized letters, alphanumeric and non-alphanumeric characters")
//    @Size(min = 1, max = 300, message = "Characters length in name can be only in between 1 and 300 inclusive")
    private String name;

    private String note;

    /**
     * Množství = počet položek (typ závisí na kindOfUnit).
     *
     * <i>Datový typ {@link Integer} je to proto, aby bylo možné zadat hodnotu null do DB, dle toho se pozná, že
     * uživatel nezadal žádné množství. Kdyby byl například int apod. Byla by vždy minimálně jako výchozí hodnota 0, ale
     * v takovém případě by to znamenalo, že uživatel vždy zadal množství nula.</i>
     */
//    @Min(value = 0, message = "Amount must be greater than or equal to 1")
//    @Max(value = Integer.MAX_VALUE, message = "Amount must be less or equal to " + Integer.MAX_VALUE)
    private Integer amount;

    @Enumerated(EnumType.STRING)
    private KindOfUnit kindOfUnit;

    @Enumerated(EnumType.STRING)
    private Priority priority;

    //    @NotNull(message = "Kind of item must not be null")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private KindOfShoppingItem kindOfShoppingItem;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime deadline;

    @Column(nullable = false)
    private boolean done;

    //    @NotNull(message = "List must not be null (item must be in a specific list)")
    @ManyToOne(optional = false)
    @JsonBackReference// Zde při serializaci nebudou data
    private ShoppingList list;
}