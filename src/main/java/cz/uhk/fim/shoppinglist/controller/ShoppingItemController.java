package cz.uhk.fim.shoppinglist.controller;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.service.ShoppingItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * CRUD operace pro položky nákupního listu.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 12.03.2019 23:43
 */

@RestController
@RequestMapping("/shopping-item")
@Validated// Aby spring spustil validaci u metod pomocí u příslušných parametrů uvedených anotací
public class ShoppingItemController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingItemController.class);

    private final ShoppingItemService shoppingItemService;

    @Autowired
    public ShoppingItemController(ShoppingItemService shoppingItemService) {
        this.shoppingItemService = shoppingItemService;
    }


    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ShoppingItem add(/*@Valid*/ @RequestBody ShoppingItem shoppingItem) {
        LOGGER.info("Add shopping item. Item: {}", shoppingItem);
        return shoppingItemService.add(shoppingItem);
    }


    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ShoppingItem update(/*@Valid*/ @RequestBody ShoppingItem shoppingItem) {
        LOGGER.info("Update shopping item. New item: {}", shoppingItem);
        return shoppingItemService.update(shoppingItem);
    }


    @DeleteMapping(value = "/{id}")
    public void delete(@NotNull @Min(1) @PathVariable Long id) {
        LOGGER.info("Delete shopping item by it´s id ('{}')", id);
        shoppingItemService.delete(id);
    }


    @GetMapping(value = "/by-list-id/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Iterable<ShoppingItem> getItemsByShoppingListId(@NotNull @Min(1) @PathVariable Long id) {
        LOGGER.info("Get all shopping items by list id {} from DB.", id);
        return shoppingItemService.getItemsByShoppingListId(id);
    }


    @DeleteMapping("/delete-by-list-id/{id}")
    public void deleteAllItemsByListId(@NotNull @Min(1) @PathVariable Long id) {
        LOGGER.info("Delete all shopping items by list id {}.", id);
        shoppingItemService.deleteItemsByListId(id);
    }
}