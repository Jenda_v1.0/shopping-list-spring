package cz.uhk.fim.shoppinglist.controller;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.service.ShoppingListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * CRUD pro nákupní list.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 18:44
 */

@RestController
@RequestMapping("/shopping-list")
@Validated
public class ShoppingListController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingListController.class);

    private final ShoppingListService shoppingListService;

    @Autowired
    public ShoppingListController(ShoppingListService shoppingListService) {
        this.shoppingListService = shoppingListService;
    }


    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ShoppingList add(/*@Valid*/ @RequestBody ShoppingList shoppingList) {
        LOGGER.info("Add shopping list. List: {}", shoppingList);
        return shoppingListService.add(shoppingList);
    }


    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ShoppingList update(/*@Valid*/ @RequestBody ShoppingList shoppingList) {
        LOGGER.info("Update shopping item. Item: {}", shoppingList);
        return shoppingListService.update(shoppingList);
    }


    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Iterable<ShoppingList> getAll() {
        LOGGER.info("Get all shopping lists from DB.");
        return shoppingListService.getAll();
    }


    @DeleteMapping("/{id}")
    public void delete(@NotNull @Min(1) @PathVariable Long id) {
        LOGGER.info("Delete shopping list (and it´s items) by it´s id ('{}')", id);
        shoppingListService.delete(id);
    }


    @DeleteMapping("/all")
    public void deleteAll() {
        LOGGER.info("Delete all shopping lists");
        shoppingListService.deleteAll();
    }


    @PostMapping("/is-name-used")
    public boolean isListNameUsed(@RequestParam @NotBlank String name) {
        LOGGER.info("Check whether the list name is already in use or not. List name: {}", name);
        return shoppingListService.isListNameUsed(name);
    }


    @PostMapping(value = "/is-list-used", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public boolean isListNameUsedExcept(/*@Valid*/ @RequestBody ShoppingList list) {
        LOGGER.info("Check whether the list name is already in use or not. Test will not include a list in the parameter. List: {}", list);
        return shoppingListService.isListNameUsed(list);
    }
}