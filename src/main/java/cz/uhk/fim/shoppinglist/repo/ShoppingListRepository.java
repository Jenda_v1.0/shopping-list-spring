package cz.uhk.fim.shoppinglist.repo;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Pro vykonávání CRUD operací nad nákupním listem {@link cz.uhk.fim.shoppinglist.entity.ShoppingList} v db.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 12.03.2019 23:49
 */

public interface ShoppingListRepository extends JpaRepository<ShoppingList, Long> {

    /**
     * Zjištění, kolik listů s názvem name již existuje.
     *
     * <i>Mělo by se vždy vrátit pouze 0 nebo 1, takže by příkaz šel napsat i tak, aby se vracely přímo ty položky
     * ({@link ShoppingList}) nebo jejich list apod.</i>
     *
     * @param name
     *         název listu, o kterém se má zjistit, kolikrát se v DB nachází.
     *
     * @return počet položek (listů) s názvem name.
     */
    @Query("select count(list) from ShoppingList list where list.name = :name")
    int getCountOfListsWithName(@Param("name") String name);

    /**
     * zjištění, kolik listů s názvem name existuje v DB. Do testování nebude zahrnut list s id id.
     *
     * @param name
     *         název listu, o kterém se má zjistit, zdali list s tímto názvem již v DB existuje nebo ne.
     * @param id
     *         list s názvem name (jedna položka {@link ShoppingList}), list v DB, který má toto id nebude do testování
     *         shody názvu zahrnut.
     *
     * @return počet položek (listů), které nemají id id a obsahující název name.
     */
    @Query("select count(list) from ShoppingList list where list.name = :name and list.id <> :id")
    int getCountOfListsWithName(@Param("name") String name, @Param("id") Long id);
}