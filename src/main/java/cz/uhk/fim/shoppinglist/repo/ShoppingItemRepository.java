package cz.uhk.fim.shoppinglist.repo;

import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Pro vykonávání operací s nákupní položkou {@link ShoppingItem} v DB.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 19:05
 */

public interface ShoppingItemRepository extends JpaRepository<ShoppingItem, Long> {

    /**
     * Získání veškerých položek patřících do níkupního listu s id listId.
     *
     * @param listId
     *         id nákupního listu, jehož položky se mají načíst.
     *
     * @return položky nákupního listu s listId.
     */
    @Query("select item from ShoppingItem item where item.list.id = :id order by item.name")
    Iterable<ShoppingItem> findByListId(@Param("id") Long listId);

    /**
     * Smazání veškerých nákupních položek patřících do listu s id listId.
     *
     * @param listId
     *         id nákupního listu jehož položky se mají vymazat.
     */
    @Transactional
    @Modifying
    @Query("delete from ShoppingItem item where item.list.id = :id")
    void deleteItemsByListId(@Param("id") Long listId);
}