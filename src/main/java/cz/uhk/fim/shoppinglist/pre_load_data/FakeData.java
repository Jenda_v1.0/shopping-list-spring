package cz.uhk.fim.shoppinglist.pre_load_data;

import cz.uhk.fim.shoppinglist.entity.ShoppingList;

/**
 * Pro vytvoření ukázkových / falešných hodnot v DB.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 10.04.2019 11:23
 */

public interface FakeData {

    ShoppingList createShoppingListWithTasks(String name);

    ShoppingList createShoppingListWithDescriptions(String name);
}