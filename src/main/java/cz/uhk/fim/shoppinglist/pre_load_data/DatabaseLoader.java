package cz.uhk.fim.shoppinglist.pre_load_data;

import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.repo.ShoppingListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Toto není povinné a je vhodné mít data někde například v dumpu. Jedná se o výchozí vložení hodnot do DB (vlastní
 * testová data).
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 03.03.2019 19:08
 */

@Component
public class DatabaseLoader implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseLoader.class);

    private final ShoppingItemRepository shoppingItemRepository;
    private final ShoppingListRepository shoppingListRepository;

    private final FakeData fakeData;

    @Autowired
    public DatabaseLoader(ShoppingItemRepository shoppingItemRepository, ShoppingListRepository shoppingListRepository, FakeData fakeData) {
        this.shoppingItemRepository = shoppingItemRepository;
        this.shoppingListRepository = shoppingListRepository;
        this.fakeData = fakeData;
    }

    @Override
    public void run(String... args) {
        LOGGER.info("Check if database is empty to insert fake values.");

        // Pokud bude DB obsahovat nějaká data, žádná se vkládat nebudou:
        if (!shoppingItemRepository.findAll().isEmpty() || !shoppingListRepository.findAll().isEmpty())
            return;

        LOGGER.info("Insert fake items to database.");

        fakeData.createShoppingListWithTasks("List 1");
        fakeData.createShoppingListWithTasks("List 2");
        fakeData.createShoppingListWithTasks("List 3");
        fakeData.createShoppingListWithTasks("List 4");

        fakeData.createShoppingListWithDescriptions("List 5");
        fakeData.createShoppingListWithDescriptions("List 6");
        fakeData.createShoppingListWithDescriptions("List 7");
        fakeData.createShoppingListWithDescriptions("List 8");
    }
}