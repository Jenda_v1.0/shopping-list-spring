package cz.uhk.fim.shoppinglist.pre_load_data;

import cz.uhk.fim.shoppinglist.entity.KindOfShoppingItem;
import cz.uhk.fim.shoppinglist.entity.KindOfUnit;
import cz.uhk.fim.shoppinglist.entity.ShoppingItem;
import cz.uhk.fim.shoppinglist.entity.ShoppingList;
import cz.uhk.fim.shoppinglist.repo.ShoppingItemRepository;
import cz.uhk.fim.shoppinglist.repo.ShoppingListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Vytvoření připravených / falešných hodnot pro DB.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 10.04.2019 11:24
 */

@Component
public class FakeDataImpl implements FakeData {

    private static final Logger LOGGER = LoggerFactory.getLogger(FakeDataImpl.class);

    private final ShoppingListRepository shoppingListRepository;
    private final ShoppingItemRepository shoppingItemRepository;

    @Autowired
    public FakeDataImpl(ShoppingListRepository shoppingListRepository, ShoppingItemRepository shoppingItemRepository) {
        this.shoppingListRepository = shoppingListRepository;
        this.shoppingItemRepository = shoppingItemRepository;
    }


    @Override
    public ShoppingList createShoppingListWithTasks(String name) {
        final ShoppingList shoppingList = new ShoppingList();
        final List<ShoppingItem> shoppingItems = createShoppingListTaskItems(shoppingList);
        return saveShoppingList(name, shoppingList, shoppingItems);
    }

    @Override
    public ShoppingList createShoppingListWithDescriptions(String name) {
        final ShoppingList shoppingList = new ShoppingList();
        final List<ShoppingItem> shoppingItems = createShoppingListDescriptionItems(shoppingList);
        return saveShoppingList(name, shoppingList, shoppingItems);
    }

    private List<ShoppingItem> createShoppingListTaskItems(ShoppingList list) {
        final List<ShoppingItem> items = new ArrayList<>();

        IntStream.range(0, 5).forEach(i -> {
            final ShoppingItem shoppingItem = new ShoppingItem();
            shoppingItem.setName("List item " + (i + 1));
            shoppingItem.setNote("This is a task note.");
            shoppingItem.setAmount(i);
            shoppingItem.setKindOfUnit(KindOfUnit.PIECE);
            shoppingItem.setKindOfShoppingItem(KindOfShoppingItem.TASK);
            shoppingItem.setDeadline(createDateInOneHour());
            shoppingItem.setList(list);

            items.add(shoppingItem);
            LOGGER.info("Added new shopping item {} to shopping list {}", shoppingItem, list);
        });

        return items;
    }

    private List<ShoppingItem> createShoppingListDescriptionItems(ShoppingList list) {
        final List<ShoppingItem> items = new ArrayList<>();

        IntStream.range(0, 5).forEach(i -> {
            final ShoppingItem shoppingItem = new ShoppingItem();
            shoppingItem.setName("List item " + (i + 1));
            shoppingItem.setNote("This is a description note.");
            shoppingItem.setKindOfShoppingItem(KindOfShoppingItem.DESCRIPTION);
            shoppingItem.setList(list);

            items.add(shoppingItem);
            LOGGER.info("Added new shopping item {} to shopping list {}", shoppingItem, list);
        });

        return items;
    }

    private static LocalDateTime createDateInOneHour() {
        return LocalDateTime.now().plusHours(1);
    }

    private ShoppingList saveShoppingList(String name, ShoppingList list, List<ShoppingItem> items) {
        list.setName(name);
        list.setItems(items);

        LOGGER.info("Insert list to database {}", list);

        final ShoppingList shoppingList = shoppingListRepository.save(list);
        items.forEach(shoppingItemRepository::save);
        return shoppingList;
    }
}