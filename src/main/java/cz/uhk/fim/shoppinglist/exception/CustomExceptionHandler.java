package cz.uhk.fim.shoppinglist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Obsluha zachytávání výjimek.
 *
 * <i>Pokud nastane výjimka, v této třídě se u příslušné metody obslouží v případě, že se jedná výjimku uvedenou v
 * anotaci: @{@link ExceptionHandler}</i>
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 07.03.2019 23:59
 */

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(ShoppingListNotFound.class)
    public ResponseEntity<ErrorResponse> handleListNotFoundException(ShoppingListNotFound e) {
        e.printStackTrace();

        // Zaslání zprávy na frontend:
        final ErrorResponse errorResponse = new ErrorResponse(e.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ErrorResponse> handleValidationException(ValidationException e) {
        e.printStackTrace();

        // Zaslání zprávy na frontend (i s validačními hláškami):
        final ErrorResponse errorResponse = new ErrorResponse(e.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

//    Pro validaci pomocí anotaci:
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public ResponseEntity<ErrorResponse> handleValidationException(MethodArgumentNotValidException e) {
//        e.printStackTrace();
//
//        // Zaslání zprávy na frontend (i s validačními hláškami):
//        final ErrorResponse errorResponse = prepareValidationErrorResponse(e);
//        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
//    }


    // Výchozí zachycení všech výjimek:
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorResponse> handleException(Throwable throwable) {
        throwable.printStackTrace();

        // Zaslání zprávy na frontend:
        final ErrorResponse errorResponse = new ErrorResponse(throwable.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }


//    /**
//     * Připravení odpovědi pro frontend v případě, že vypadne výjimka e.
//     *
//     * <i>Jedná se o výjimku vzniklou kvůli chybě validace hodnot.</i>
//     *
//     * <i>Projdou se veškeré chybové validace ve výjimce e a vloži se do listu v příslušné entitě a vrátí se na
//     * frontend.</i>
//     *
//     * @param e
//     *         nastalá výjimka vzniklá validační chybou.
//     *
//     * @return entitu, která se vrátí na frontend, bude obsahovat informace o veškerých proměnných obsahující validační
//     * chybu.
//     */
//    private static ErrorResponse prepareValidationErrorResponse(MethodArgumentNotValidException e) {
//        final BindingResult bindingResult = e.getBindingResult();
//        final List<FieldError> fieldErrors = bindingResult.getFieldErrors();
//
//        final ErrorResponse errorResponse = new ErrorResponse("Validation error");
//        fieldErrors.forEach(errorResponse::addFieldError);
//
//        return errorResponse;
//    }
}