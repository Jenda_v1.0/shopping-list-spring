package cz.uhk.fim.shoppinglist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Výjimka značí, že nebyl nalezen příslušný nákupní list.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 21.03.2019 22:48
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ShoppingListNotFound extends RuntimeException {

    public ShoppingListNotFound(String message) {
        super(message);
    }
}