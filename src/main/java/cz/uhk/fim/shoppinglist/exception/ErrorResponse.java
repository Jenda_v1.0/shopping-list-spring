package cz.uhk.fim.shoppinglist.exception;

import lombok.Data;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Objekt pro zasílání zpráv na frontend v případě, že dojde k výjimce.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 08.03.2019 0:01
 */

@Data
class ErrorResponse {

    /**
     * Chybová zpráva.
     */
    private final String message;

    /**
     * Seznam chybových (/ validačních) zpráv uvedených u validací, resp. u proměnných obsahující anotaci pro validaci
     * hodnoty.
     */
    private final List<String> fieldErrors;

    /**
     * Čas, kdy došlo k příslušné výjimce, resp. zaslání (konkrétně v době vytvoření příslušné instance) tohoto
     * objektu.
     */
    private final Date timeStamp;


    ErrorResponse(String message) {
        timeStamp = new Date();
        this.message = message;
        fieldErrors = new ArrayList<>();
    }


    void addFieldError(FieldError fieldError) {
        // Na frontend se bude posílat pouze zpráva pro uživatele (chyba uvedená u příslušné anotace pro validaci):
        fieldErrors.add(fieldError.getDefaultMessage());
    }
}