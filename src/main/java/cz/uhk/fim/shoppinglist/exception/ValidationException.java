package cz.uhk.fim.shoppinglist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Výjimka pro vyhození v kontroléru v případě, že dojde k nějaké chybě při validaci položek.
 *
 * @author Jan Krunčík
 * @version 1.0
 * @since 13.04.2019 16:45
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }
}