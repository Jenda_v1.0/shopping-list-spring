# shopping-list-spring

React evening school (BVP). Shoping list v Spring Boot + React. (Doplnění backendu pro https://gitlab.com/Jenda_v1.0/shopping-list)



# Úvod

Jedná se o aplikaci typu nákupní košík. V tomto smyslu byla aplikace vytvořena, ale lze ji aplikovat i v jiných oblastech, například rozdělení úkolů v domácnosti, například co list to jeden člen. Jiný způsob ve formě splnění úkolů do školy (spíše ve formě ukolníčku) atp. V aplikaci je možné vytvářet nákupní listy a pro každý nákupní list vytvořit jednotlivé položky. Položku lze rozdělit na dva typy. Může se jednat buď o položku typu "Task", která značí "úkol", že se má něco koupit, například rohlíky nebo může značit splnění "nečeho" apod. Druhý typ je "Description", který značí spíše nějakou poznámku nebo nějaký dodatek k seznamu.

Ke každé položce lze nastavit její název ("o kterou položku se jedná"), poznámku k té položce, například mléko polotučné, 5 balení atd. Dále množství, které může značit buď počet kusů nebo váhu apod. Například 5 ks ruhlíků, 5 dkg salátu apod. Priorita, kterou lze vyjádřit různě dle položky, například jak moc je důležité položku koupit, když ji například nebudou v příslušném obchodě mít, je potřeba zajet do jiného nebo je možné koupi položky odložit na jindy apod. Na konec deadline, který značít, do kdy by se položky měla koupit či splnit.


## Ovládací prvky

V horní části okna aplikace je skrolovací menu obsahující jednotlivé (nákupní) listy. Ve spodní části zmíněného menu jsou dvě tlačítka. První je pro vytvoření nového listu a druhé pro smazání veškerých listů (včetně jejich položek). U každého listu v menu je v pravé části k dispozici ikona pro změnu názvu listu nebo jeho smazání. Pod tím je pole pro zadání názvu položky, kterou chce uživatel vytvořit. Pod ním jsou tří tlačítka. První je pro vytvoření nové položky, druhé je pro vyprázdnění textového pole pro název položky. Poslední je pro vymazání veškerých položek v označeném nákupním listu.

Je možné rozkliknout / otevřit rozevírací menu s textem "Details". V takovém případě se rozevře nabídka obsahující textová pole a další grafické prvky (obecně formulář) pro zadání výše zmíněných prvků položky.

Pod rozevírací nabídkou pro data o položce je další textové pole s rozevírací nabidkou. Tyto dvě komponenty slouží pro vyfiltrování položek v označeném listu. Je možné filtrovat položky, které ve svém názvu nebo poznámce obsahují zadaný text (/filtrované klíčové slovo).

Jako poslední komponenta je "panel" obshaující 5 sloupců. V prvním sloupci jsou vždy zobrazené veškeré položky označeného listu bez ohledu na typ položky ("Task" Description"). V druhém sloupci jsou položky, které jsou typu "Task" a jsou aktivní, tedy dosud nesplněné (nemají označený CheckBox). Ve třetím sloupci jsou položky typu "Task", které jsou již splněné (mají označený CheckBox). Čtvrtý sloupec slouží pro filtrování položek dle priority a poslední sloupec pro filtrování položek dle jejich typu.

Položky označeného listu jsou v jednotlivých sloupcích panelu znázorněny jako rozevírací nabídky (pro každou položku). Po jejím rozevření můžeme vidět údaje, které jsme vyplnili ve formuláři při vytváření položky. V případě, že jsme zadali datum, které je starší než to aktuální, bude příslušné datum spolu s názvem položky zvýrazněné červeně. To značí, že uživatel nestihl splnit položku v požadovaném čase (/ termínu). Při označení položky za splněnou nebude aktuální čas testován, zdali je starší apod. Ve spodní části jsou k dispozici tlačítka pro smazání konkrétní položky (vpravo) nebo pro editaci položky (vlevo). Při zvolení editace položky bude výše popsaný formulář pro vytvoření položky naplněn údaji zvolené položky pro editaci a pod textovým polem pro název položky bude k dispozici navíc tlačítko pro uložení změn příslušné položky (bude první tlačítko - nalevo vedle tlačítka pro vytvoření nové položky).



# Docker

Pro spuštění aplikace v Dockeru jsou k dispozici dvě připravené možnosti.


## Docker compose

Spuštění aplikace v Docker konzoli pomocí Docker Compose. Slouží pro nastartování aplikace i s databází. Princip je v souborech `Dockerfile` a `docker-compose.yml`, kde se druhý uvedený soubor stará mimo jiné hlavně o vyžádání startu databáze a poté je možné spustit aplikaci dle `Dockerfile`.


### Postup

- Vhodné nastavit přeskočení testů
- Nejprve je vhodné v souboru `shopping-list/src/todo_list/server/calls.js` odkomentovat (nejsou li) adresy URL bez localhostu ve funkcích `getShoppingItemUri` a `getShoppingListUri`. Jedná se o adresy, kde je v komentáři uvedeno, že se jedná o adresy pro Docker.
- V `application.properties` je třeba si odkomentovat (/ nastavit) připojení k databází, které je definováno v `.yml` souboru. jedná se o příkaz: `spring.datasource.url=jdbc:mysql://db:3306/shopping_list...`, je u toho uveden komentář pro spuštění v Dockeru.

1. Maven clean (doporučeno)
    - Vymaže adresář `target`
2. Maven Build and copy frontend
    - Vytvoří build React aplikace (frontend) a zkopíruje potřebné soubory do adresáře `/resources/static`
3. Maven package
    - Sestaví spustitelný `.jar` soubor, který potřebuje Docker pro načtení.
4. Docker
    1. V Docker konzoli najet do adresáře projektu
    2. V Docker konzoli zavolat příkaz "docker-compose up"
5. Ve webovém prohlížeči zadat IP adresu Dockeru v následující podobě:
    1. http://ip_adresa_dockeru:8080


## Vytvoření Docker image pomocí Maven pluginu

V tomto případě se jedná zejména o spuštění aplikace samotné bez primárního vytvoření databáze nebo připojení k ní. Pro tyto účely je připraveno připojení k H2 (embednuté) databázi.


### Postup

Postup je podobný předchozímu. Nejprve je třeba provést odrážy uvedené u předchozího postupu a změna nastane až u souboru `application.properties`, kde je potřeba odkomentovat řádky uvedené u komentáře `H2`. Jedná se o řádky uvedené níže. A připravené / výchozí URL k databázi je třeba zakomentovat (nechat pouze tu pro H2).

- V `pom.xml` souboru je třeba odkomentovat závislost pro H2 databázi a zakomentovat výchozí závislost pro připojení k MySQL databázi. 

**Příkazy pro připojení k H2 databází:**

```
spring.h2.console.enabled=true
spring.h2.console.path=/h2
spring.datasource.url=jdbc:h2:file:~/shopping_list
spring.datasource.driver-class-name=org.h2.Driver
```

1. Maven clean (doporučeno)
2. Maven Build and copy frontend
3. Zkopírovat obsah souboru `Dockerfile_SpotifyImage` do souboru `Dockerfile`
    - Soubor `Dockerfile_SpotifyImage` obsahuje připravené příkazy pro sestavení Docker image, ale Docker využívá pouze soubor s názvem `Dockerfile`, proto toto zkopírování obsahu
    - Je to z toho důvodu, aby bylo možné aplikaci spustit buď "samotnou" nebo "celou" (i s databází)
3. Maven Docker build
    - Slouží pro zkopírování `Dockerfile` souboru do adresáře `target` a pro předání názvu spustitelného `.jar` souboru, který má `Dockerfile` spustit (aplikaci)
4. Maven package
5. Docker
    1. Ve Virtual Boxu nastavit pravidlo pro přeposílání požadavků z portu 8080 prohlížeče na port 8080 aplikace
    2. V Docker konzoli najet do adresáře projektu
    3. Vytvořit image pomoci Docker konzole zavoláním příkazu `mvn package dockerfile:build`
        - Volitelně (spiše doporučeno) je možné vynechat testy příkazem `mvn package dockerfile:build -Dmaven.test.skip=true`
    4. Image je možné spustit příkazem `docker run -i -t image_id`



# MIT License

Copyright (c) 2019 Jan Krunčík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.